import re
from UploadFileClient import UploadFileClient
import logging
import boto3
import os

class ProcessAHRIFiles:

    def __init__(self, ac_identifier, hp_identifier, furnace_identifier, ule_identifier, boilers_identifier):
        self.dataOrchestration = boto3.client('datapipeline')
        self.ac_identifier = ac_identifier
        self.hp_identifier = hp_identifier
        self.furnace_identifier = furnace_identifier
        self.ule_identifier = ule_identifier
        self.boilers_identifier = boilers_identifier

    def remove_ahri_file_from_local(self, ahriFilePath):
        for file in os.listdir(ahriFilePath):
            if file.endswith(".zip") or file.endswith(".CSV") or file.endswith(".csv") or file.endswith(".log") or file.endswith(".html"):
                os.remove(ahriFilePath + file)
        
    def remove_ahri_processed_files_from_s3(self,awsAccessKey, awsSecretAccessKey, procBucket):
        self.logger = logging.getLogger()
        s3_client = boto3.client('s3')
        BUCKET = procBucket
        PREFIX = 'conduit_processed/'
        response = s3_client.list_objects_v2(Bucket=BUCKET, Prefix=PREFIX)
        for object in response['Contents']:
            print('Deleting', object['Key'])
            s3_client.delete_object(Bucket=BUCKET, Key=object['Key'])


    def preProccessModels(self, UnitModel, compileExpression):
        UnitModel = UnitModel.replace('"', '')
        UnitModel = UnitModel.replace(' ', '')
        matching_string = re.finditer(compileExpression, UnitModel)
        for matched_expression in matching_string:
            if matched_expression.group(2):
                UnitModel = UnitModel.replace(matched_expression.group(2), '*')
        return UnitModel

    def processMatchedElements(self, matchedString, compileExpression):
        recursiveCall = 0
        optionsArray = []
        start = matchedString.group(1)
        variablePath = matchedString.group(2)
        variableElements = variablePath.split(',')
        end = matchedString.group(3)
        callRec = re.finditer(compileExpression, end)
        for recursiveIter in callRec:
            if recursiveIter:
                recursiveCall = 1
                recursiveMatch = self.processMatchedElements(recursiveIter, compileExpression)
                # variableElements = variablePath.split(',')
                for element in variableElements:
                    for recElement in recursiveMatch:
                        optionsArray.append(start + element + recElement)
            else:
                break
        if recursiveCall == 0:
            # splitStr = ',' if variablePath.find(',') != -1 else '.'
            variableElements = variablePath.split(',')
            for element in variableElements:
                optionsArray.append(start + element + end)
        return optionsArray

    def processOutDoorIndoorAndFurnace(self, outDoorModels, inDoorModels, furnaceModels, outDoorsOriginalData,
                                       inDoorsOriginalData, furnaceOriginalData, line):
        newFileContent = ''
        dataContent = line
        for outDoorModel in outDoorModels:
            dataContent = dataContent.replace(outDoorsOriginalData, outDoorModel)
            outDoorsOriginalData = outDoorModel
            for inDoorModel in inDoorModels:
                dataContent = dataContent.replace(inDoorsOriginalData, inDoorModel)
                inDoorsOriginalData = inDoorModel
                for furnaceModel in furnaceModels:
                    dataContent = dataContent.replace(furnaceOriginalData, furnaceModel)
                    furnaceOriginalData = furnaceModel
                    newFileContent += self.processLineToinsetion(dataContent)
        return newFileContent

    def processOutDoorIndoor(self, outDoorModels, inDoorModels, outDoorsOriginalData, inDoorsOriginalData, line):
        newFileContent = ''
        dataContent = line
        for outDoorModel in outDoorModels:
            dataContent = dataContent.replace(outDoorsOriginalData, outDoorModel)
            outDoorsOriginalData = outDoorModel
            for inDoorModel in inDoorModels:
                dataContent = dataContent.replace(inDoorsOriginalData, inDoorModel)
                inDoorsOriginalData = inDoorModel
                newFileContent += self.processLineToinsetion(dataContent)
        return newFileContent

    def processOutDoorFurnace(self, outDoorModels, furnaceModels, outDoorsOriginalData, furnaceOriginalData, line):
        newFileContent = ''
        dataContent = line
        for outDoorModel in outDoorModels:
            dataContent = dataContent.replace(outDoorsOriginalData, outDoorModel)
            outDoorsOriginalData = outDoorModel
            for furnaceModel in furnaceModels:
                dataContent = dataContent.replace(furnaceOriginalData, furnaceModel)
                furnaceOriginalData = furnaceModel
                newFileContent += self.processLineToinsetion(dataContent)

        return newFileContent

    def processIndoorFurnace(self, inDoorModels, furnaceModels, inDoorsOriginalData, furnaceOriginalData, line):
        newFileContent = ''
        dataContent = line
        for inDoorModel in inDoorModels:
            dataContent = dataContent.replace(inDoorsOriginalData, inDoorModel)
            inDoorsOriginalData = inDoorModel
            for furnaceModel in furnaceModels:
                dataContent = dataContent.replace(furnaceOriginalData, furnaceModel)
                furnaceOriginalData = furnaceModel
                newFileContent += self.processLineToinsetion(dataContent)

        return newFileContent

    def processOnlyOutDoor(self, outDoorModels, outDoorsOriginalData, line):
        newFileContent = ''
        dataContent = line
        for outDoorModel in outDoorModels:
            dataContent = dataContent.replace(outDoorsOriginalData, outDoorModel)
            outDoorsOriginalData = outDoorModel
            newFileContent += self.processLineToinsetion(dataContent)
        return newFileContent

    def processOnlyInDoor(self, inDoorModels, inDoorsOriginalData, line):
        newFileContent = ''
        dataContent = line
        for inDoorModel in inDoorModels:
            dataContent = dataContent.replace(inDoorsOriginalData, inDoorModel)
            inDoorsOriginalData = inDoorModel
            newFileContent += self.processLineToinsetion(dataContent)
        return newFileContent

    def processOnlyFurnace(self, furnaceModels, furnaceOriginalData, line):
        newFileContent = ''
        dataContent = line
        for furnaceModel in furnaceModels:
            dataContent = dataContent.replace(furnaceOriginalData, furnaceModel)
            furnaceOriginalData = furnaceModel
            newFileContent += self.processLineToinsetion(dataContent)
        return newFileContent

    def processLineToinsetion(self, lineString):
        lineStringProcessed = lineString

        lineStringProcessed = lineStringProcessed.replace('"', '')
        lineStringProcessed = lineStringProcessed.replace(',', '.')
        lineStringProcessed = lineStringProcessed.replace('|', ',')
        lineStringProcessed = lineStringProcessed.replace('Active', '1', 1)
        lineStringProcessed = lineStringProcessed.replace('Discontinued', '-2', 1)
        lineStringProcessed = lineStringProcessed.replace('Obsolete', '-1', 1)
        lineStringProcessed = lineStringProcessed.replace('Production Stopped', '0', 1)

        return lineStringProcessed + '\n'

    def defineTypeOfBoilerFile(self, key):
        comercialSystemPrefix = 'CBLR'
        dataIndexIfFound = key.find(comercialSystemPrefix)
        if (dataIndexIfFound > 0):
            return 0
        else:
            return 1

    def defineTypeofFileToImport(self, key):
        miniSplitSystemPrefix = 'VSMS'
        dataIndexIfFound = key.find(miniSplitSystemPrefix)
        if (dataIndexIfFound > 0):
            return 0
        else:
            historicPrefix = '_H_'
            dataIndexIfFound = key.find(historicPrefix)
            if (dataIndexIfFound > 0):
                return 2
            else:
                return 1

    def processCSVBody(self, data_body, outdoor_model_position, indoor_model_position, furnace_model_position, system_type, type_of_file, pre_process_compiled_exp, process_compiled_exp):
        newfileContent = ''

        for line in data_body:
            addThisLine = line.replace('","', '"|"')

            lineExplodeToDefineElements = addThisLine.split('|')

            if system_type == self.ule_identifier:
                addThisLine = self.modifyULERegularLine(lineExplodeToDefineElements)
            elif system_type == self.boilers_identifier and type_of_file == 0:
                addThisLine = self.modifyCBLRFile(lineExplodeToDefineElements)

            if system_type != self.boilers_identifier:
                addThisLine = addThisLine.replace('"|"', '"|'+ str(system_type) +'|"', 1)

            outdoorUnitModel = ''
            if outdoor_model_position != 0:
                originalOutdoorUnitModel = lineExplodeToDefineElements[outdoor_model_position]
                outdoorUnitModel = self.preProccessModels(originalOutdoorUnitModel, pre_process_compiled_exp)
                addThisLine = addThisLine.replace(originalOutdoorUnitModel, outdoorUnitModel)

            indoorUnitModel = ''
            if indoor_model_position != 0:
                originalIndoorUnitModel = lineExplodeToDefineElements[indoor_model_position]
                indoorUnitModel = self.preProccessModels(originalIndoorUnitModel, pre_process_compiled_exp)
                indoorUnitModel = indoorUnitModel.replace(" ", "")
                addThisLine = addThisLine.replace(originalIndoorUnitModel, indoorUnitModel)

            furnaceModel = ''
            if furnace_model_position != 0:
                originalFurnaceModel = lineExplodeToDefineElements[furnace_model_position]
                furnaceModel = self.preProccessModels(originalFurnaceModel, pre_process_compiled_exp)
                furnaceModel = furnaceModel.replace(" ", "")
                addThisLine = addThisLine.replace(originalFurnaceModel, furnaceModel)

            outdoorUnitModelMatchExpression = re.finditer(process_compiled_exp, outdoorUnitModel)
            outDoorModels = []
            for outdoorUnitModelSingleExpression in outdoorUnitModelMatchExpression:
                newLinesStringsOutdoors = self.processMatchedElements(outdoorUnitModelSingleExpression,
                                                                      process_compiled_exp)
                outDoorModels.extend(newLinesStringsOutdoors)

            indoorUnitModelMatchExpression = re.finditer(process_compiled_exp, indoorUnitModel)
            inDoorModels = []
            for indoorUnitModelSingleExpression in indoorUnitModelMatchExpression:
                newLinesStringsIndoors = self.processMatchedElements(indoorUnitModelSingleExpression, process_compiled_exp)
                inDoorModels.extend(newLinesStringsIndoors)

            furnaceModelMatchExpression = re.finditer(process_compiled_exp, furnaceModel)
            furnaceModels = []
            for furnaceUnitModelSingleExpression in furnaceModelMatchExpression:
                newLinesStringsFurnace = self.processMatchedElements(furnaceUnitModelSingleExpression,
                                                                     process_compiled_exp)
                furnaceModels.extend(newLinesStringsFurnace)

            if len(outDoorModels) > 0 and len(inDoorModels) > 0 and len(furnaceModels) > 0:
                processDataLines = self.processOutDoorIndoorAndFurnace(outDoorModels, inDoorModels, furnaceModels,
                                                                       outdoorUnitModel, indoorUnitModel, furnaceModel,
                                                                       addThisLine)
                newfileContent += processDataLines

            elif len(outDoorModels) > 0 and len(inDoorModels) > 0:
                processDataLines = self.processOutDoorIndoor(outDoorModels, inDoorModels, outdoorUnitModel,
                                                             indoorUnitModel, addThisLine)
                newfileContent += processDataLines

            elif len(outDoorModels) > 0 and len(furnaceModels) > 0:
                processDataLines = self.processOutDoorFurnace(outDoorModels, furnaceModels, outdoorUnitModel,
                                                              furnaceModel, addThisLine)
                newfileContent += processDataLines

            elif len(inDoorModels) > 0 and len(furnaceModels) > 0:
                processDataLines = self.processIndoorFurnace(inDoorModels, furnaceModels, indoorUnitModel, furnaceModel,
                                                             addThisLine)
                newfileContent += processDataLines

            elif len(outDoorModels) > 0:
                processDataLines = self.processOnlyOutDoor(outDoorModels, outdoorUnitModel, addThisLine)
                newfileContent += processDataLines

            elif len(inDoorModels) > 0:
                processDataLines = self.processOnlyInDoor(inDoorModels, indoorUnitModel, addThisLine)
                newfileContent += processDataLines

            elif len(furnaceModels) > 0:
                processDataLines = self.processOnlyFurnace(furnaceModels, furnaceModel, addThisLine)
                newfileContent += processDataLines

            else:
                newfileContent += self.processLineToinsetion(addThisLine)

        return newfileContent

    def defineModelsNumbersPositions(self, typeOfFile, system_type):
        # structure [outdoor, indoor, furnace], fixed positions per file where er find the models numbers
        models_positions = []
        #Straigth cool
        if system_type == self.ac_identifier:
            #models_positions = [6, 8, 12]
            models_positions = [7, 9, 13]
        #Heat Pump
        elif system_type == self.hp_identifier:
            if typeOfFile == 0:
                #models_positions = [6, 9, 0]
                models_positions = [7, 10, 0]
            else:
                #models_positions = [6, 8, 12]
                models_positions = [7, 9, 13]
        #Furnace
        elif system_type == self.furnace_identifier:
            #models_positions = [0, 0, 4]
            models_positions = [0, 0, 5]
        #ULE
        elif system_type == self.ule_identifier:
            #models_positions = [5, 6, 0]
            models_positions = [6, 7, 0]
        #BOILERS
        elif system_type == self.boilers_identifier:
            #models_positions = [0, 0, 4]
            models_positions = [0, 0, 5]

        return models_positions

    def modifyULERegularLine(self, lineArray):
        firstTimeIteration = 1
        newLine = ''

        #coilQuantity = lineArray[16]
        coilQuantity = lineArray[17]
        lineArray.remove(coilQuantity)
        #lineArray.insert(8, coilQuantity)
        lineArray.insert(9, coilQuantity)

        #capacity = lineArray[9]
        capacity = lineArray[10]
        searchPattern = '/'
        dataIndexIfFound = capacity.find(searchPattern)
        if (dataIndexIfFound > 0):
            capacityReal, elRep = capacity.split("/")
            capacityReal = capacityReal.replace('"', '')
        else:
            capacityReal = capacity
        #lineArray[9] = capacityReal
        lineArray[10] = capacityReal

        #IEE = lineArray[11]
        IEE = lineArray[12]
        searchPattern = '/'
        dataIndexIfFound = IEE.find(searchPattern)
        if (dataIndexIfFound > 0):
            IEEReal, elRep = IEE.split("/")
            IEEReal = IEEReal.replace('"', '')
        else:
            IEEReal = IEE

        lineArray.remove(IEE)
        #lineArray.insert(14, IEEReal)
        lineArray.insert(15, IEEReal)

        #EER = lineArray[10]
        EER = lineArray[11]
        searchPattern = '/'
        dataIndexIfFound = EER.find(searchPattern)
        if (dataIndexIfFound > 0):
            EERReal, elRep = EER.split("/")
            EERReal = EERReal.replace('"', '')
        else:
            EERReal = EER

        #lineArray[10] = EERReal
        lineArray[11] = EERReal

        for fileData in lineArray:
            if (firstTimeIteration == 1):
                newLine += fileData
                firstTimeIteration = 0
            else:
                newLine += '|' + fileData
        return newLine

    def modifyCBLRFile(self, lineArray):
        spacesForSteam = 0
        firstTimeIteration = 1
        newLine = ''

        #switchDataWater = lineArray[13]
        switchDataWater = lineArray[14]
        #switchDataSteam = lineArray[14]
        switchDataSteam = lineArray[15]
        #lineArray[13] = switchDataSteam
        lineArray[14] = switchDataSteam
        #lineArray[14] = switchDataWater
        lineArray[15] = switchDataWater

        for fileData in lineArray:
            if (firstTimeIteration == 1):
                newLine += fileData
                firstTimeIteration = 0
            else:
                newLine += '|' + fileData
        return newLine

    def processFiles(self, file_name, awsAccessKey, awsSecretAccessKey, procBucket, system_type, log_results_file_name, create_single_file):
        logger = logging.getLogger()
        logger.info("Received event: Processing " + file_name)

        try:
            if os.path.isfile(file_name):
                
                if os.stat(file_name).st_size == 0:
                     pass
                else:
                    response = open(file_name, 'r')
                    
                    fileContent = response.read()
                    contentArray = fileContent.splitlines()
                    contentArray.pop(0)
                    preProcessCompileExpression = re.compile(ur'(.*)(\(\d\))$', re.M | re.I)
                    compileExpression = re.compile(r'([]*[\w\d]*?[*]*?[-|+]*?[\w\d]*?[\w\d]*[-|+]?[\w\d]*)[[|(](.*?)[]|)](.*)?',
                                                re.M | re.I)
                    if system_type == self.boilers_identifier:
                        typeOfFile = self.defineTypeOfBoilerFile(file_name)
                    else:
                        typeOfFile = self.defineTypeofFileToImport(file_name)

                    models_positions = self.defineModelsNumbersPositions(typeOfFile, system_type)

                    newfileContent = self.processCSVBody(contentArray, models_positions[0], models_positions[1],
                                                        models_positions[2], system_type, typeOfFile, preProcessCompileExpression, compileExpression)
                    proc_name = ''
                    if create_single_file == 1:
                        proc_name = log_results_file_name
                        if os.path.isfile(proc_name):
                            procfile = open(proc_name, 'a')
                            procfile.write(newfileContent)
                            procfile.close()
                        else:
                            procfile = open(proc_name, 'w')
                            procfile.write(newfileContent)
                            procfile.close()
                    else:
                        proc_name = file_name.replace('.', '_ALL_PROC.')
                        procfile = open(proc_name, 'w')
                        procfile.write(newfileContent)
                        procfile.close()

                    cloud_name = proc_name.split('/')
                    cloud_name = cloud_name[len(cloud_name)-1]

                    self.uploadFileToS3Bucket(proc_name, awsAccessKey, awsSecretAccessKey, procBucket, 'conduit_processed/'+cloud_name)

                return 1
        except Exception as e:
            print(e)
            raise e

    def uploadFileToS3Bucket(self, filepath, awsAccessKey, awsSecretAccessKey, procBucket, fileName):
        logger = logging.getLogger()
        procfile = open(filepath, 'r')
        logger.info('finished process of file ')
        logger.info('Starting the upload of file '+fileName)
        uploadFileClient = UploadFileClient(awsAccessKey, awsSecretAccessKey)
        if uploadFileClient.uploadFileToS3(procfile, procBucket, fileName):
            logger.info('file uploaded')
        else:
            logger.info('The upload file failed')
