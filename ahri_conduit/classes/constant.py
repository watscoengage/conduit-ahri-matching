from airflow.models import Variable

# COMMON VARS WITH OTHERS PROJECTS DON'T TOUCH

AWS_ACCESS_KEY_ID = Variable.get('aws_access_key_id')
AWS_SECRET_ACCESS_KEY = Variable.get('aws_secret_access_key')
BUCKET = Variable.get('bucket_ahri_files')
BUCKET_ARCHIVE = Variable.get('bucket_ahri_archive')

AC_IDENTIFIER = Variable.get('ac_files_identifier')
HP_IDENTIFIER = Variable.get('hp_files_identifier')
FURNACE_IDENTIFIER = Variable.get('furnace_file_identifier')
ULE_IDENTIFIER = Variable.get('ule_files_identifier')
BOILERS_IDENTIFIER = Variable.get('boilers_file_identifier')

# CONDUIT Vars

CONDUIT_TMP_FILE_PATH = Variable.get('ahri_files_conduit_temp_path')
CONDUIT_TMP_LOG_PATCH = Variable.get('process_log_conduit_files_path')

CONDUIT_DB_READ_ENDPOINT = Variable.get('conduit_db_read_endpoint')
CONDUIT_DB_READ_USER = Variable.get('conduit_db_read_user')
CONDUIT_DB_READ_PASSWORD = Variable.get('conduit_db_read_password')
CONDUIT_DB_READ_NAME = Variable.get('conduit_db_read_name')

CONDUIT_DB_WRITE_ENDPOINT = Variable.get('conduit_db_write_endpoint')
CONDUIT_DB_WRITE_USER = Variable.get('conduit_db_write_user')
CONDUIT_DB_WRITE_PASSWORD = Variable.get('conduit_db_write_password')
CONDUIT_DB_WRITE_NAME = Variable.get('conduit_db_write_name')

AHRI_REPORT_USERS_LIST = Variable.get('ahri_weekly_report_users_list')

CONDUIT_PIPELINES_DICT_PATH = '/home/ec2-user/airflow/dags/ahri_conduit/pipelines/'

CONDUIT_UPGRADE_GENERAL_MODEL = 'CONDUIT_UPGRADE_GENERAL_MODEL'
CONDUIT_UPGRADE_MODELS_AC = 'CONDUIT_UPGRADE_MODELS_AC'
CONDUIT_UPGRADE_MODELS_AC_VSMAC = 'CONDUIT_UPGRADE_MODELS_AC_VSMAC'
CONDUIT_UPGRADE_MODELS_HP = 'CONDUIT_UPGRADE_MODELS_HP'
CONDUIT_UPGRADE_MODELS_HP_VSMSHP = 'CONDUIT_UPGRADE_MODELS_HP_VSMSHP'
CONDUIT_UPGRADE_MODELS_BOILER_RBLR = 'CONDUIT_UPGRADE_MODELS_BOILER_RBLR'
CONDUIT_UPGRADE_MODELS_BOILER_CBLR = 'CONDUIT_UPGRADE_MODELS_BOILER_CBLR'
CONDUIT_UPGRADE_MODELS_ULE = 'CONDUIT_UPGRADE_MODELS_ULE'
CONDUIT_UPGRADE_MODELS_FURNACE = 'CONDUIT_UPGRADE_MODELS_FURNACE'

CONDUIT_NOTIFICATION_ERROR_LIST = Variable.get('conduit_notification_error_list')

