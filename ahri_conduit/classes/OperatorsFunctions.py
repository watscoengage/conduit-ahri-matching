import zipfile
import logging
import constant as constant
import os
from EquipmentProcessingClient import ProcessAHRIFiles
from BaseSupportSetupClient import BaseSetupProcess
from AHRIConduitGeneralReport import ConduitReportWrap
from UploadFileClient import DownloadFileClient
from AHRIMappingFilesLayer import FilesACMaps, FileHPMap, FileBoilersMap, FileUleMap, FileFurnaceMap
from ManagePipeline import ManagePipeline

logger = logging.getLogger()

process_equipment_instance = ProcessAHRIFiles(constant.AC_IDENTIFIER, constant.HP_IDENTIFIER, constant.FURNACE_IDENTIFIER, constant.ULE_IDENTIFIER, constant.BOILERS_IDENTIFIER)

enterprise_connection_instance = BaseSetupProcess(constant.CONDUIT_DB_WRITE_ENDPOINT, constant.CONDUIT_DB_WRITE_USER, constant.CONDUIT_DB_WRITE_PASSWORD, constant.CONDUIT_DB_WRITE_NAME)

enterprise_report_instance = ConduitReportWrap(constant.CONDUIT_DB_READ_ENDPOINT, constant.CONDUIT_DB_READ_USER, constant.CONDUIT_DB_READ_PASSWORD, constant.CONDUIT_DB_READ_NAME, constant.CONDUIT_TMP_LOG_PATCH)

manage_pipeline_instance = ManagePipeline(constant.CONDUIT_DB_WRITE_ENDPOINT, constant.CONDUIT_DB_WRITE_USER, constant.CONDUIT_DB_WRITE_PASSWORD, constant.CONDUIT_DB_WRITE_NAME, constant.CONDUIT_PIPELINES_DICT_PATH)


class OperatorsFunctions:
    # --------------------------------- METHODS ----------------------------------------------------------------------------
    @staticmethod
    def get_files_from_bucket():
        # Reading files from constant.BUCKET
        files_to_download = ['ACfile1.zip', 'HPfiles.zip', 'RFRfiles.zip', 'VSMSACfiles.zip', 'VSMSHPfiles.zip',
                             'ULEfiles.zip', 'RBLRfiles.zip', 'CBLRfiles.zip']
        s3_instance_object = DownloadFileClient(constant.AWS_ACCESS_KEY_ID, constant.AWS_SECRET_ACCESS_KEY)
        s3_instance_object.downloadFilesFromS3('ahri-import', 'enterprise-extractions', files_to_download,
                                               constant.CONDUIT_TMP_FILE_PATH)

        # after download all files unzip the ones that we initially need
        # fixed files array key=>values pairs as: 'name of the zip file'=>'name of the file'
        files_array = {constant.CONDUIT_TMP_FILE_PATH + 'ACfile1.zip': 'AC_1_Active.CSV',
                        constant.CONDUIT_TMP_FILE_PATH + 'ACfile2.zip': 'AC_1_Obsolete_.CSV',                        
                        constant.CONDUIT_TMP_FILE_PATH + 'ACfile2.zip': 'AC_2_.CSV',
                        constant.CONDUIT_TMP_FILE_PATH + 'ACfile3.zip': 'AC_3_.CSV',
                        constant.CONDUIT_TMP_FILE_PATH + 'HPfiles.zip': 'HP_1_.CSV',
                        constant.CONDUIT_TMP_FILE_PATH + 'HPfiles.zip': 'HP_1_Obsolete.CSV', 
                        constant.CONDUIT_TMP_FILE_PATH + 'RFRfiles.zip': 'RFR_1_Active.CSV',
                        constant.CONDUIT_TMP_FILE_PATH + 'RFRfiles.zip': 'RFR_1_Obsolete.CSV',
                        constant.CONDUIT_TMP_FILE_PATH + 'VSMSACfiles.zip': 'VSMSAC_1_Active.CSV',
                        constant.CONDUIT_TMP_FILE_PATH + 'VSMSACfiles.zip': 'VSMSAC_1_Obsolete.CSV',
                        constant.CONDUIT_TMP_FILE_PATH + 'VSMSHPfiles.zip': 'VSMSHP_1_Active.CSV', 
                        constant.CONDUIT_TMP_FILE_PATH + 'VSMSHPfiles.zip': 'VSMSHP_1_Obsolete.CSV', 
                        constant.CONDUIT_TMP_FILE_PATH + 'ULEfiles.zip': 'ULE_1_Active.CSV',
                        constant.CONDUIT_TMP_FILE_PATH + 'ULEfiles.zip': 'ULE_1_Obsolete.CSV',
                        constant.CONDUIT_TMP_FILE_PATH + 'RBLRfiles.zip': 'RBLR_1_Active.CSV',
                        constant.CONDUIT_TMP_FILE_PATH + 'RBLRfiles.zip': 'RBLR_1_Obsolete.CSV',
                        constant.CONDUIT_TMP_FILE_PATH + 'CBLRfiles.zip': 'CBLR_1_Active.CSV',
                        constant.CONDUIT_TMP_FILE_PATH + 'CBLRfiles.zip': 'CBLR_1_Obsolete.CSV'}

        for zip_file, csv_file in files_array.items():
            if zipfile.is_zipfile(zip_file):
                logger.info('unzip file ' + zip_file)

                logger.info('extracting file ' + csv_file)
                read_zip_file_content = zipfile.ZipFile(zip_file, 'r')
                read_zip_file_content.extractall(constant.CONDUIT_TMP_FILE_PATH)
                read_zip_file_content.close()

        logger.info('The files has been downloaded and unzipped')

    # --------------------------------- START Mapping Layer ----------------------------------------------------------------
    # This tasks bulk will perform a map btw the new file received and the old data received.
    @staticmethod
    def ac_mapping_new_source():
        # AC and VSMSAC
        mapping_ac_instance = FilesACMaps(constant.CONDUIT_TMP_FILE_PATH + 'AC_1_Active.CSV', constant.CONDUIT_TMP_FILE_PATH + 'VSMSAC_1_Active.CSV',constant.CONDUIT_TMP_FILE_PATH + 'AC_1_Obsolete.CSV', constant.CONDUIT_TMP_FILE_PATH + 'VSMSAC_1_Obsolete.CSV') 
               
        if os.path.exists(constant.CONDUIT_TMP_FILE_PATH + 'AC_1_Active.CSV') and os.stat(constant.CONDUIT_TMP_FILE_PATH + 'AC_1_Active.CSV').st_size == 0:
            pass
        else:
            mapping_ac_instance.mapACFile()
        if os.path.exists(constant.CONDUIT_TMP_FILE_PATH + 'VSMSAC_1_Active.CSV') and os.stat(constant.CONDUIT_TMP_FILE_PATH + 'VSMSAC_1_Active.CSV').st_size == 0:
            pass
        else:    
            mapping_ac_instance.mapVSMSACFile()
        if os.path.exists(constant.CONDUIT_TMP_FILE_PATH + 'AC_1_Obsolete.CSV') and os.stat(constant.CONDUIT_TMP_FILE_PATH + 'AC_1_Obsolete.CSV').st_size == 0:
            pass
        else:
            mapping_ac_instance.mapACOBSFile()
        if os.path.exists(constant.CONDUIT_TMP_FILE_PATH + 'VSMSAC_1_Obsolete.CSV') and os.stat(constant.CONDUIT_TMP_FILE_PATH + 'VSMSAC_1_Obsolete.CSV').st_size == 0:
            pass
        else:
            mapping_ac_instance.mapVSMSACOBSFile()
        

    @staticmethod
    def hp_mapping_new_source():
        # HP and VSMSHP
        mapping_hp_instance = FileHPMap(constant.CONDUIT_TMP_FILE_PATH + 'HP_1_Active.CSV', constant.CONDUIT_TMP_FILE_PATH + 'VSMSHP_1_Active.CSV',constant.CONDUIT_TMP_FILE_PATH + 'HP_1_Obsolete.CSV', constant.CONDUIT_TMP_FILE_PATH + 'VSMSHP_1_Obsolete.CSV')

        if os.path.exists(constant.CONDUIT_TMP_FILE_PATH + 'HP_1_Active.CSV') and os.stat(constant.CONDUIT_TMP_FILE_PATH + 'HP_1_Active.CSV').st_size == 0:
            pass
        else:
            mapping_hp_instance.mapHPFile()
        if os.path.exists(constant.CONDUIT_TMP_FILE_PATH + 'VSMSHP_1_Active.CSV') and os.stat(constant.CONDUIT_TMP_FILE_PATH + 'VSMSHP_1_Active.CSV').st_size == 0:
            pass
        else:
            mapping_hp_instance.mapVSMSHPFile()
        if os.path.exists(constant.CONDUIT_TMP_FILE_PATH + 'HP_1_Obsolete.CSV') and os.stat(constant.CONDUIT_TMP_FILE_PATH + 'HP_1_Obsolete.CSV').st_size == 0:
            pass
        else:
            mapping_hp_instance.mapHPOBSFile()
        if os.path.exists(constant.CONDUIT_TMP_FILE_PATH + 'VSMSHP_1_Obsolete.CSV') and os.stat(constant.CONDUIT_TMP_FILE_PATH + 'VSMSHP_1_Obsolete.CSV').st_size == 0:
            pass
        else:
            mapping_hp_instance.mapVSMSHPOBSFile()
            
      

    @staticmethod
    def boiler_mapping_new_source():
        # Boiler CBRL and RBLR
        mapping_bl_instance = FileBoilersMap(constant.CONDUIT_TMP_FILE_PATH + 'CBLR_1_Active.CSV', constant.CONDUIT_TMP_FILE_PATH + 'RBLR_1_Active.CSV',constant.CONDUIT_TMP_FILE_PATH + 'CBLR_1_Obsolete.CSV', constant.CONDUIT_TMP_FILE_PATH + 'RBLR_1_Obsolete.CSV')
        
        if os.path.exists(constant.CONDUIT_TMP_FILE_PATH + 'CBLR_1_Active.CSV') and os.stat(constant.CONDUIT_TMP_FILE_PATH + 'CBLR_1_Active.CSV').st_size == 0:
            pass
        else:
            mapping_bl_instance.mapCBLRfile()
        if os.path.exists(constant.CONDUIT_TMP_FILE_PATH + 'RBLR_1_Active.CSV') and os.stat(constant.CONDUIT_TMP_FILE_PATH + 'RBLR_1_Active.CSV').st_size == 0:
            pass
        else:
            mapping_bl_instance.mapRBLRFile()
        if os.path.exists(constant.CONDUIT_TMP_FILE_PATH + 'CBLR_1_Obsolete.CSV') and os.stat(constant.CONDUIT_TMP_FILE_PATH + 'CBLR_1_Obsolete.CSV').st_size == 0:
            pass
        else:
            mapping_bl_instance.mapCBLROBSFile()
        if os.path.exists(constant.CONDUIT_TMP_FILE_PATH + 'RBLR_1_Obsolete.CSV') and os.stat(constant.CONDUIT_TMP_FILE_PATH + 'RBLR_1_Obsolete.CSV').st_size == 0:
            pass
        else:
            mapping_bl_instance.mapRBLROBSFile()
        

    @staticmethod
    def furnace_mapping_new_source():
        # TODO Furnace
        mapping_furnace_instance = FileFurnaceMap(constant.CONDUIT_TMP_FILE_PATH + 'RFR_1_Active.CSV',constant.CONDUIT_TMP_FILE_PATH + 'RFR_1_Obsolete.CSV')
        
        if os.path.exists(constant.CONDUIT_TMP_FILE_PATH + 'RFR_1_Active.CSV') and os.stat(constant.CONDUIT_TMP_FILE_PATH + 'RFR_1_Active.CSV').st_size == 0:
            pass
        else:
             mapping_furnace_instance.mapRFRNfile()
        if os.path.exists(constant.CONDUIT_TMP_FILE_PATH + 'RFR_1_Obsolete.CSV') and os.stat(constant.CONDUIT_TMP_FILE_PATH + 'RFR_1_Obsolete.CSV').st_size == 0:
            pass
        else:
             mapping_furnace_instance.mapRFRNOBSfile()

    @staticmethod
    def ule_mapping_new_source():
        # ULE
        mapping_ule_instance = FileUleMap(constant.CONDUIT_TMP_FILE_PATH + 'ULE_1_Active.CSV',constant.CONDUIT_TMP_FILE_PATH + 'ULE_1_Obsolete.CSV')


        if os.path.exists(constant.CONDUIT_TMP_FILE_PATH + 'ULE_1_Active.CSV') and os.stat(constant.CONDUIT_TMP_FILE_PATH + 'ULE_1_Active.CSV').st_size == 0:
            pass
        else:
             mapping_ule_instance.mapULEfile()
        if os.path.exists(constant.CONDUIT_TMP_FILE_PATH + 'ULE_1_Obsolete.CSV') and os.stat(constant.CONDUIT_TMP_FILE_PATH + 'ULE_1_Obsolete.CSV').st_size == 0:
            pass
        else:
             mapping_ule_instance.mapULEOBSfile()

    # --------------------------------- END Mapping Layer ------------------------------------------------------------------

    # -----------------------------------START CLEANS PROCESS---------------------------------------------------------------
    # This task pre-process runs for each file. This task cleans all the models numbers and upload results to s3.
    # This process uses the files with the suffix _MAP, and create a new file with _ALL_PROCESSED suffix to upload.
    @staticmethod
    def ac_process_equipment():
        # this task will process AC equipment and upload results to s3
        ac_files_array = [constant.CONDUIT_TMP_FILE_PATH + 'AC_MAP_1_.CSV', constant.CONDUIT_TMP_FILE_PATH + 'AC_MAP_2_.CSV',
                          constant.CONDUIT_TMP_FILE_PATH + 'AC_MAP_3_.CSV']
        for csv_file in ac_files_array:
            if os.path.isfile(csv_file):
                process_equipment_instance.processFiles(csv_file, constant.AWS_ACCESS_KEY_ID, constant.AWS_SECRET_ACCESS_KEY, constant.BUCKET,
                                                    constant.AC_IDENTIFIER, constant.CONDUIT_TMP_FILE_PATH + 'AC_ALL_PROCESSED.CSV', 1)

        vsms_csv_file = constant.CONDUIT_TMP_FILE_PATH + 'VSMSAC_MAP_1_.CSV'
        if os.path.isfile(vsms_csv_file):
            process_equipment_instance.processFiles(vsms_csv_file, constant.AWS_ACCESS_KEY_ID, constant.AWS_SECRET_ACCESS_KEY, constant.BUCKET,
                                                constant.AC_IDENTIFIER, constant.CONDUIT_TMP_FILE_PATH + 'VSMS_AC_PROCESSED.CSV', 0)

    @staticmethod
    def hp_process_equipment():
        # this task will process HP equipment and upload results to s3
        hp_files_array = [constant.CONDUIT_TMP_FILE_PATH + 'HP_MAP_1_.CSV', constant.CONDUIT_TMP_FILE_PATH + 'VSMSHP_MAP_1_.CSV']
        for csv_file in hp_files_array:
            if os.path.isfile(csv_file):
                process_equipment_instance.processFiles(csv_file, constant.AWS_ACCESS_KEY_ID, constant.AWS_SECRET_ACCESS_KEY, constant.BUCKET,
                                                    constant.HP_IDENTIFIER, constant.CONDUIT_TMP_FILE_PATH + 'HP_ALL_PROCESSED.CSV', 0)

    @staticmethod
    def f_process_equipment():
        # this task will process Furnace equipment and upload results to s3
        furnace_file = constant.CONDUIT_TMP_FILE_PATH + 'RFR_MAP_1_.CSV'
        if os.path.isfile(furnace_file):
            process_equipment_instance.processFiles(furnace_file, constant.AWS_ACCESS_KEY_ID, constant.AWS_SECRET_ACCESS_KEY, constant.BUCKET,
                                                constant.FURNACE_IDENTIFIER, constant.CONDUIT_TMP_FILE_PATH + 'FR_ALL_PROCESSED.CSV', 0)

    @staticmethod
    def b_process_equipment():
        # this task will process boilers equipment and upload results to s3
        boilers_file_array = [constant.CONDUIT_TMP_FILE_PATH + 'CBLR_MAP_1_.CSV', constant.CONDUIT_TMP_FILE_PATH + 'RBLR_MAP_1_.CSV']
        for file in boilers_file_array:
            if os.path.isfile(file):
                process_equipment_instance.processFiles(file, constant.AWS_ACCESS_KEY_ID, constant.AWS_SECRET_ACCESS_KEY, constant.BUCKET,
                                                    constant.BOILERS_IDENTIFIER, constant.CONDUIT_TMP_FILE_PATH + 'BL_ALL_PROCESSED.CSV', 0)

    @staticmethod
    def ule_process_equipment():
        # this task will parse the ULE and upload results to s3
        ule_file = constant.CONDUIT_TMP_FILE_PATH + 'ULE_MAP_1_.CSV'
        if os.path.isfile(ule_file):
            process_equipment_instance.processFiles(ule_file, constant.AWS_ACCESS_KEY_ID, constant.AWS_SECRET_ACCESS_KEY, constant.BUCKET,
                                                constant.ULE_IDENTIFIER, constant.CONDUIT_TMP_FILE_PATH + 'ULE_ALL_PROCESSED.CSV', 0)

    # -----------------------------------END CLEANS PROCESS-----------------------------------------------------------------

    # ------------------------------- START Conduit Process ----------------------------------------------------------------
    @staticmethod
    def setup_conduit_support():
        # this task will prepare enterprise DB to receive the updates files
        enterprise_connection_instance.CreateConduitDatabaseSupport()

    @staticmethod
    def upgrade_general_models_for_conduit():
        logger.info('Setting up the operational base into Enterprise database')
        pipeline_id = manage_pipeline_instance.startPipeline(constant.CONDUIT_UPGRADE_GENERAL_MODEL)
        logger.info('Pipeline ' + constant.CONDUIT_UPGRADE_GENERAL_MODEL + ' succesfully initialized')
        manage_pipeline_instance.CheckPipelineExecution(pipeline_id)

    @staticmethod
    def upgrade_models_for_conduit_ac():
        # this task will trigger pipeline to insert ac to enterprise
        # conduit_pipeline_id = Variable.get('insert_ac_general_to_enterprise_pipeline_id')
        # logger.info('Setting up the operational base into Enterprise database')
        # enterprise_connection_instance.StartPielineProcess(conduit_pipeline_id)
        # logger.info('Pipeline ' + str(conduit_pipeline_id) + ' succesfully initialized')
        # # will wait untill the pipeline is finished
        # enterprise_connection_instance.CheckPipelineExecution(conduit_pipeline_id)
        if os.path.isfile(constant.CONDUIT_TMP_FILE_PATH + 'AC_ALL_PROCESSED.CSV'):
            if (os.stat(constant.CONDUIT_TMP_FILE_PATH + 'AC_ALL_PROCESSED.CSV').st_size == 0):
                pass
            else:
                logger.info('Setting up the operational base into Enterprise database')
                logger.info('Pipeline ' + constant.CONDUIT_UPGRADE_MODELS_AC + ' succesfully initialized')
                pipeline_id = manage_pipeline_instance.startPipeline(constant.CONDUIT_UPGRADE_MODELS_AC)
                logger.info('Pipeline ' + constant.CONDUIT_UPGRADE_MODELS_AC + ' succesfully initialized')
                manage_pipeline_instance.CheckPipelineExecution(pipeline_id)

    @staticmethod
    def upgrade_models_for_conduit_ac_vsmsac():
        # this task will trigger pipeline to insert vsmsac to enterprise
        # conduit_pipeline_id = Variable.get('insert_vsmsac_general_to_enterprise_pipeline_id')
        # logger.info('Setting up the operational base into Enterprise database')
        #
        # enterprise_connection_instance.StartPielineProcess(conduit_pipeline_id)
        # logger.info('Pipeline ' + str(conduit_pipeline_id) + ' succesfully initialized')
        # # will wait untill the pipeline is finished
        # enterprise_connection_instance.CheckPipelineExecution(conduit_pipeline_id)
        if os.path.isfile(constant.CONDUIT_TMP_FILE_PATH + 'VSMSAC_MAP_1__ALL_PROC.CSV'):
            if (os.stat(constant.CONDUIT_TMP_FILE_PATH + 'VSMSAC_MAP_1__ALL_PROC.CSV').st_size == 0):
                pass
            else:
                
                logger.info('Pipeline ' + constant.CONDUIT_UPGRADE_MODELS_AC_VSMAC + ' succesfully initialized')
                pipeline_id = manage_pipeline_instance.startPipeline(constant.CONDUIT_UPGRADE_MODELS_AC_VSMAC)
                logger.info('Pipeline ' + constant.CONDUIT_UPGRADE_MODELS_AC_VSMAC + ' succesfully initialized')
                manage_pipeline_instance.CheckPipelineExecution(pipeline_id)

    @staticmethod
    def upgrade_models_for_conduit_hp():
        # this task will trigger pipeline to insert hp to enterprise
        # conduit_pipeline_id = Variable.get('insert_hp_general_to_enterprise_pipeline_id')
        # logger.info('Setting up the operational base into Enterprise database')
        #
        # enterprise_connection_instance.StartPielineProcess(conduit_pipeline_id)
        # logger.info('Pipeline ' + str(conduit_pipeline_id) + ' succesfully initialized')
        # # will wait untill the pipeline is finished
        # enterprise_connection_instance.CheckPipelineExecution(conduit_pipeline_id)
        if os.path.isfile(constant.CONDUIT_TMP_FILE_PATH + 'HP_MAP_1__ALL_PROC.CSV'):
            if (os.stat(constant.CONDUIT_TMP_FILE_PATH + 'HP_MAP_1__ALL_PROC.CSV').st_size == 0):
                pass
            else:
                
                logger.info('Pipeline ' + constant.CONDUIT_UPGRADE_MODELS_HP + ' succesfully initialized')
                pipeline_id = manage_pipeline_instance.startPipeline(constant.CONDUIT_UPGRADE_MODELS_HP)
                logger.info('Pipeline ' + constant.CONDUIT_UPGRADE_MODELS_HP + ' succesfully initialized')
                manage_pipeline_instance.CheckPipelineExecution(pipeline_id)

    @staticmethod
    def upgrade_models_for_conduit_hp_vsmshp():
        # this task will trigger pipeline to insert vsmshp to enterprise
        # conduit_pipeline_id = Variable.get('insert_vsmshp_general_to_enterprise_pipeline_id')
        # logger.info('Setting up the operational base into Enterprise database')
        #
        # enterprise_connection_instance.StartPielineProcess(conduit_pipeline_id)
        # logger.info('Pipeline ' + str(conduit_pipeline_id) + ' succesfully initialized')
        # # will wait untill the pipeline is finished
        # enterprise_connection_instance.CheckPipelineExecution(conduit_pipeline_id)
        if os.path.isfile(constant.CONDUIT_TMP_FILE_PATH + 'VSMSHP_MAP_1__ALL_PROC.CSV'):
            if (os.stat(constant.CONDUIT_TMP_FILE_PATH + 'VSMSHP_MAP_1__ALL_PROC.CSV').st_size == 0):
                pass
            else:
                
                logger.info('Pipeline ' + constant.CONDUIT_UPGRADE_MODELS_HP_VSMSHP + ' succesfully initialized')
                pipeline_id = manage_pipeline_instance.startPipeline(constant.CONDUIT_UPGRADE_MODELS_HP_VSMSHP)
                logger.info('Pipeline ' + constant.CONDUIT_UPGRADE_MODELS_HP_VSMSHP + ' succesfully initialized')
                manage_pipeline_instance.CheckPipelineExecution(pipeline_id)

    @staticmethod
    def upgrade_models_for_conduit_r_blr():
        # this task will trigger pipelines to update, insert or set to obsolete
        # conduit_pipeline_id = Variable.get('insert_rblr_general_to_enterprise_pipeline_id')
        # logger.info('Setting up the operational base into Enterprise database')
        #
        # enterprise_connection_instance.StartPielineProcess(conduit_pipeline_id)
        # logger.info('Pipeline ' + str(conduit_pipeline_id) + ' succesfully initialized')
        # # will wait untill the pipeline is finished
        # enterprise_connection_instance.CheckPipelineExecution(conduit_pipeline_id)
        if os.path.isfile(constant.CONDUIT_TMP_FILE_PATH + 'RBLR_MAP_1__ALL_PROC.CSV'):
            if (os.stat(constant.CONDUIT_TMP_FILE_PATH + 'RBLR_MAP_1__ALL_PROC.CSV').st_size == 0):
                pass
            else:
                
                logger.info('Pipeline ' + constant.CONDUIT_UPGRADE_MODELS_BOILER_RBLR + ' succesfully initialized')
                pipeline_id = manage_pipeline_instance.startPipeline(constant.CONDUIT_UPGRADE_MODELS_BOILER_RBLR)
                logger.info('Pipeline ' + constant.CONDUIT_UPGRADE_MODELS_BOILER_RBLR + ' succesfully initialized')
                manage_pipeline_instance.CheckPipelineExecution(pipeline_id)

    @staticmethod
    def upgrade_models_for_conduit_c_blr():
        # this task will trigger pipelines to update, insert or set to obsolete
        # conduit_pipeline_id = Variable.get('insert_cblr_general_to_enterprise_pipeline_id')
        # logger.info('Setting up the operational base into Enterprise database')
        #
        # enterprise_connection_instance.StartPielineProcess(conduit_pipeline_id)
        # logger.info('Pipeline ' + str(conduit_pipeline_id) + ' succesfully initialized')
        # # will wait untill the pipeline is finished
        # enterprise_connection_instance.CheckPipelineExecution(conduit_pipeline_id)
        if os.path.isfile(constant.CONDUIT_TMP_FILE_PATH + 'CBLR_MAP_1__ALL_PROC.CSV'):        
            if (os.stat(constant.CONDUIT_TMP_FILE_PATH + 'CBLR_MAP_1__ALL_PROC.CSV').st_size == 0):
                pass
            else:
                
                logger.info('Pipeline ' + constant.CONDUIT_UPGRADE_MODELS_BOILER_CBLR + ' succesfully initialized')
                pipeline_id = manage_pipeline_instance.startPipeline(constant.CONDUIT_UPGRADE_MODELS_BOILER_CBLR)
                logger.info('Pipeline ' + constant.CONDUIT_UPGRADE_MODELS_BOILER_CBLR + ' succesfully initialized')
                manage_pipeline_instance.CheckPipelineExecution(pipeline_id)

    @staticmethod
    def upgrade_models_for_conduit_ule():
        # this task will trigger pipelines to update, insert or set to obsolete
        # conduit_pipeline_id = Variable.get('insert_ule_general_to_enterprise_pipeline_id')
        # logger.info('Setting up the operational base into Enterprise database')
        #
        # enterprise_connection_instance.StartPielineProcess(conduit_pipeline_id)
        # logger.info('Pipeline ' + str(conduit_pipeline_id) + ' succesfully initialized')
        # # will wait untill the pipeline is finished
        # enterprise_connection_instance.CheckPipelineExecution(conduit_pipeline_id)
        if os.path.isfile(constant.CONDUIT_TMP_FILE_PATH + 'ULE_MAP_1__ALL_PROC.CSV'):        
            if (os.stat(constant.CONDUIT_TMP_FILE_PATH + 'ULE_MAP_1__ALL_PROC.CSV').st_size == 0):
                pass
            else:
                
                logger.info('Pipeline ' + constant.CONDUIT_UPGRADE_MODELS_ULE + ' succesfully initialized')
                pipeline_id = manage_pipeline_instance.startPipeline(constant.CONDUIT_UPGRADE_MODELS_ULE)
                logger.info('Pipeline ' + constant.CONDUIT_UPGRADE_MODELS_ULE + ' succesfully initialized')
                manage_pipeline_instance.CheckPipelineExecution(pipeline_id)

    @staticmethod
    def upgrade_models_for_conduit_furnace():
        # this task will trigger pipelines to update, insert or set to obsolete
        # conduit_pipeline_id = Variable.get('insert_furnace_general_to_enterprise_pipeline_id')
        # logger.info('Setting up the operational base into Enterprise database')
        #
        # enterprise_connection_instance.StartPielineProcess(conduit_pipeline_id)
        # logger.info('Pipeline ' + str(conduit_pipeline_id) + ' succesfully initialized')
        # # will wait untill the pipeline is finished
        # enterprise_connection_instance.CheckPipelineExecution(conduit_pipeline_id)
        if os.path.isfile(constant.CONDUIT_TMP_FILE_PATH + 'RFR_MAP_1__ALL_PROC.CSV'):        
            if (os.stat(constant.CONDUIT_TMP_FILE_PATH + 'RFR_MAP_1__ALL_PROC.CSV').st_size == 0):
                pass
            else:
                
                logger.info('Pipeline ' + constant.CONDUIT_UPGRADE_MODELS_FURNACE + ' succesfully initialized')
                pipeline_id = manage_pipeline_instance.startPipeline(constant.CONDUIT_UPGRADE_MODELS_FURNACE)
                logger.info('Pipeline ' + constant.CONDUIT_UPGRADE_MODELS_FURNACE + ' succesfully initialized')
                manage_pipeline_instance.CheckPipelineExecution(pipeline_id)

    @staticmethod
    def create_enterprise_report():
        # this task will read the results of the operation and will create an email with a operation result report for enterprise
        # creating conduit general report
        enterprise_report_instance.createHTMLReportsConduit()
        # upload files to s3 historic constant.BUCKET
        enterprise_report_instance.upload_file_to_s3(constant.AWS_ACCESS_KEY_ID, constant.AWS_SECRET_ACCESS_KEY, constant.CONDUIT_TMP_FILE_PATH,
                                                     constant.CONDUIT_TMP_LOG_PATCH,
                                                     constant.BUCKET, constant.BUCKET_ARCHIVE)

    # -------------------------------START Clean ---------------------------------------------------------------------------
    @staticmethod
    def clean_up_workspace():
        # this task will delete ahri files, process log files, and all TMP resources on the databases
        # remove files generated in this process
        process_equipment_instance.remove_ahri_file_from_local(constant.CONDUIT_TMP_FILE_PATH)
        process_equipment_instance.remove_ahri_file_from_local(constant.CONDUIT_TMP_LOG_PATCH)

        # remove Conduit Support tables
        enterprise_connection_instance.removeReportsTable()
        # removing file reports
        enterprise_report_instance.removeFileContent()

        #removing files from S3 conduit_processed folder after processing is done
        process_equipment_instance.remove_ahri_processed_files_from_s3(constant.AWS_ACCESS_KEY_ID, constant.AWS_SECRET_ACCESS_KEY,constant.BUCKET)

    # -------------------------------END Clean -----------------------------------------------------------------------------

