import pymysql
import boto3
import time
import logging
import sys
import random

class BaseSetupProcess:
    def __init__(self,databaseEndpoint, databaseUser, databasePassword, databaseName):
        self.dataOrchestration = boto3.client('datapipeline')
        self.databaseEndpoint = databaseEndpoint
        self.databaseUser = databaseUser
        self.databasePassword = databasePassword
        self.databaseName = databaseName
        self.logger = logging.getLogger()
    
    def connectToDatabase(self, databaseStack):
        # rds settings to connect to the database
        rds_host = self.databaseEndpoint
        name = self.databaseUser
        password = self.databasePassword
        db_name = self.databaseName

        try:
            conn = pymysql.connect(rds_host, user=name, passwd=password, db=db_name, connect_timeout=5)
            self.logger.info("SUCCESS: Connection to " + databaseStack + " succeeded")
            return conn
        except:
            self.logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
            sys.exit()

    def CreateConduitDatabaseSupport(self):
        conn = self.connectToDatabase('Enterprise')
        with conn.cursor() as creation_insert_report:
            create_report_table = "CREATE TABLE `AHRI_INSERTION_REPORT` ( " \
                                  "`id` int(11) unsigned NOT NULL AUTO_INCREMENT, " \
                                  "`AhriCertifiedRef` int(11) DEFAULT '0', " \
                                  "`OldAhriCertifiedRef` int(11) DEFAULT NULL, " \
                                  "`SystemType` int(11) DEFAULT '0', " \
                                  "`ModelStatus` int(11) DEFAULT '0', " \
                                  "`ManufacturedType` varchar(255) DEFAULT NULL, " \
                                  "`BrandName` longtext, " \
                                  "`SeriesName` varchar(255) DEFAULT NULL, " \
                                  "`OutdoorUnitManufactured` varchar(255) DEFAULT NULL, " \
                                  "`OutdoorUnitModel` varchar(80) DEFAULT NULL, " \
                                  "`IndoorType` varchar(70) DEFAULT NULL, " \
                                  "`IndoorUnitManufactured` varchar(255) DEFAULT NULL, " \
                                  "`IndoorUnitModel` varchar(80) DEFAULT NULL, " \
                                  "`Refrigerant` varchar(80) DEFAULT NULL, " \
                                  "`IndoorAirQuantity` int(11) DEFAULT '0', " \
                                  "`IndoorAirQuantity2` int(11) DEFAULT '0', " \
                                  "`IndoorAirQuantity3` int(11) DEFAULT '0', " \
                                  "`FurnaceModel` varchar(80) DEFAULT NULL, " \
                                  "`FurnaceManufactured` varchar(255) DEFAULT NULL, " \
                                  "`Capacity` int(11) DEFAULT '0', " \
                                  "`EER` decimal(7,4) DEFAULT '0.0000', " \
                                  "`SEER` decimal(7,4) DEFAULT '0.0000', " \
                                  "`HighHeatingCapacity` int(11) DEFAULT '0', " \
                                  "`HighHeatingCapacity_COP` int(11) DEFAULT NULL, " \
                                  "`HSPF` decimal(7,4) DEFAULT '0.0000', " \
                                  "`LowHeatingCapacity` int(11) DEFAULT '0', " \
                                  "`LowHeatingCapacity_COP` int(11) DEFAULT NULL, " \
                                  "`IEER` decimal(7,4) DEFAULT '0.0000', " \
                                  "`Phase` int(11) DEFAULT '0', " \
                                  "`AHRIType` varchar(80) DEFAULT NULL, " \
                                  "`HSVTC` varchar(3) DEFAULT NULL, " \
                                  "`ExclusiveForExport` varchar(3) DEFAULT NULL, " \
                                  "`CoolingCost` int(11) DEFAULT '0', " \
                                  "`HeatingCost` int(11) DEFAULT '0', " \
                                  "`EstNationalCoolingCost` int(11) DEFAULT '0', " \
                                  "`ModelNumber` varchar(80) DEFAULT NULL, " \
                                  "`ManufacturedHousing` varchar(255) DEFAULT NULL, " \
                                  "`FuelType` varchar(80) DEFAULT NULL, " \
                                  "`Configuration` varchar(80) DEFAULT NULL, " \
                                  "`FurnaceType` varchar(80) DEFAULT NULL, " \
                                  "`InputRating` int(11) DEFAULT '0', " \
                                  "`OutputHeatingCapacity` int(11) DEFAULT '0', " \
                                  "`AFUE` decimal(8,4) DEFAULT '0.0000', " \
                                  "`Ef` decimal(8,4) DEFAULT '0.0000', " \
                                  "`Eae` int(11) DEFAULT '0', " \
                                  "`PE` int(11) DEFAULT '0', " \
                                  "`BasicModel` varchar(3) DEFAULT NULL, " \
                                  "`ElectricallyCommutatedMotor` varchar(20) DEFAULT NULL, " \
                                  "`ElegibleForFederalTaxCredit` varchar(20) DEFAULT NULL, " \
                                  "`UpdateDate` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, " \
                                  "PRIMARY KEY (`id`) " \
                                  ") ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 MAX_ROWS=4294967295; "
            creation_insert_report.execute(create_report_table)

        with conn.cursor() as creation_updated_report:
            create_report_table_updated = "CREATE TABLE `AHRI_UPDATED_REPORT` ( " \
                                  "`id` int(11) unsigned NOT NULL AUTO_INCREMENT, " \
                                  "`AhriCertifiedRef` int(11) DEFAULT '0', " \
                                  "`OldAhriCertifiedRef` int(11) DEFAULT NULL, " \
                                  "`SystemType` int(11) DEFAULT '0', " \
                                  "`ModelStatus` int(11) DEFAULT '0', " \
                                  "`newModelStatus` int(11) DEFAULT '0', " \
                                  "`ManufacturedType` varchar(255) DEFAULT NULL, " \
                                  "`newManufacturedType` varchar(255) DEFAULT NULL, " \
                                  "`BrandName` longtext, " \
                                  "`newBrandName` longtext, " \
                                  "`SeriesName` varchar(255) DEFAULT NULL, " \
                                  "`newSeriesName` varchar(255) DEFAULT NULL, " \
                                  "`OutdoorUnitManufactured` varchar(255) DEFAULT NULL, " \
                                  "`newOutdoorUnitManufactured` varchar(255) DEFAULT NULL, " \
                                  "`OutdoorUnitModel` varchar(80) DEFAULT NULL, " \
                                  "`newOutdoorUnitModel` varchar(80) DEFAULT NULL, " \
                                  "`IndoorType` varchar(70) DEFAULT NULL, " \
                                  "`newIndoorType` varchar(70) DEFAULT NULL, " \
                                  "`IndoorUnitManufactured` varchar(255) DEFAULT NULL, " \
                                  "`newIndoorUnitManufactured` varchar(255) DEFAULT NULL, " \
                                  "`IndoorUnitModel` varchar(80) DEFAULT NULL, " \
                                  "`newIndoorUnitModel` varchar(80) DEFAULT NULL, " \
                                  "`Refrigerant` varchar(80) DEFAULT NULL, " \
                                  "`newRefrigerant` varchar(80) DEFAULT NULL, " \
                                  "`IndoorAirQuantity` int(11) DEFAULT '0', " \
                                  "`newIndoorAirQuantity` int(11) DEFAULT '0', " \
                                  "`IndoorAirQuantity2` int(11) DEFAULT '0', " \
                                  "`newIndoorAirQuantity2` int(11) DEFAULT '0', " \
                                  "`IndoorAirQuantity3` int(11) DEFAULT '0', " \
                                  "`newIndoorAirQuantity3` int(11) DEFAULT '0', " \
                                  "`FurnaceModel` varchar(80) DEFAULT NULL, " \
                                  "`newFurnaceModel` varchar(80) DEFAULT NULL, " \
                                  "`FurnaceManufactured` varchar(255) DEFAULT NULL, " \
                                  "`newFurnaceManufactured` varchar(255) DEFAULT NULL, " \
                                  "`Capacity` int(11) DEFAULT '0', " \
                                  "`newCapacity` int(11) DEFAULT '0', " \
                                  "`EER` decimal(7,4) DEFAULT '0.0000', " \
                                  "`newEER` decimal(7,4) DEFAULT '0.0000', " \
                                  "`SEER` decimal(7,4) DEFAULT '0.0000', " \
                                  "`newSEER` decimal(7,4) DEFAULT '0.0000', " \
                                  "`HighHeatingCapacity` int(11) DEFAULT '0', " \
                                  "`newHighHeatingCapacity` int(11) DEFAULT '0', " \
                                  "`HighHeatingCapacity_COP` int(11) DEFAULT NULL, " \
                                  "`newHighHeatingCapacity_COP` int(11) DEFAULT NULL, " \
                                  "`HSPF` decimal(7,4) DEFAULT '0.0000', " \
                                  "`newHSPF` decimal(7,4) DEFAULT '0.0000', " \
                                  "`LowHeatingCapacity` int(11) DEFAULT '0', " \
                                  "`newLowHeatingCapacity` int(11) DEFAULT '0', " \
                                  "`LowHeatingCapacity_COP` int(11) DEFAULT NULL, " \
                                  "`newLowHeatingCapacity_COP` int(11) DEFAULT NULL, " \
                                  "`IEER` decimal(7,4) DEFAULT '0.0000', " \
                                  "`newIEER` decimal(7,4) DEFAULT '0.0000', " \
                                  "`Phase` int(11) DEFAULT '0', " \
                                  "`newPhase` int(11) DEFAULT '0', " \
                                  "`AHRIType` varchar(80) DEFAULT NULL, " \
                                  "`newAHRIType` varchar(80) DEFAULT NULL, " \
                                  "`HSVTC` varchar(3) DEFAULT NULL, " \
                                  "`newHSVTC` varchar(3) DEFAULT NULL, " \
                                  "`ExclusiveForExport` varchar(3) DEFAULT NULL, " \
                                  "`newExclusiveForExport` varchar(3) DEFAULT NULL, " \
                                  "`CoolingCost` int(11) DEFAULT '0', " \
                                  "`newCoolingCost` int(11) DEFAULT '0', " \
                                  "`HeatingCost` int(11) DEFAULT '0', " \
                                  "`newHeatingCost` int(11) DEFAULT '0', " \
                                  "`EstNationalCoolingCost` int(11) DEFAULT '0', " \
                                  "`newEstNationalCoolingCost` int(11) DEFAULT '0', " \
                                  "`ModelNumber` varchar(80) DEFAULT NULL, " \
                                  "`newModelNumber` varchar(80) DEFAULT NULL, " \
                                  "`ManufacturedHousing` varchar(255) DEFAULT NULL, " \
                                  "`newManufacturedHousing` varchar(255) DEFAULT NULL, " \
                                  "`FuelType` varchar(80) DEFAULT NULL, " \
                                  "`newFuelType` varchar(80) DEFAULT NULL, " \
                                  "`Configuration` varchar(80) DEFAULT NULL, " \
                                  "`newConfiguration` varchar(80) DEFAULT NULL, " \
                                  "`FurnaceType` varchar(80) DEFAULT NULL, " \
                                  "`newFurnaceType` varchar(80) DEFAULT NULL, " \
                                  "`InputRating` int(11) DEFAULT '0', " \
                                  "`newInputRating` int(11) DEFAULT '0', " \
                                  "`OutputHeatingCapacity` int(11) DEFAULT '0', " \
                                  "`newOutputHeatingCapacity` int(11) DEFAULT '0', " \
                                  "`AFUE` decimal(8,4) DEFAULT '0.0000', " \
                                  "`newAFUE` decimal(8,4) DEFAULT '0.0000', " \
                                  "`Ef` decimal(8,4) DEFAULT '0.0000', " \
                                  "`newEf` decimal(8,4) DEFAULT '0.0000', " \
                                  "`Eae` int(11) DEFAULT '0', " \
                                  "`newEae` int(11) DEFAULT '0', " \
                                  "`PE` int(11) DEFAULT '0', " \
                                  "`newPE` int(11) DEFAULT '0', " \
                                  "`BasicModel` varchar(3) DEFAULT NULL, " \
                                  "`newBasicModel` varchar(3) DEFAULT NULL, " \
                                  "`ElectricallyCommutatedMotor` varchar(20) DEFAULT NULL, " \
                                  "`newElectricallyCommutatedMotor` varchar(20) DEFAULT NULL, " \
                                  "`ElegibleForFederalTaxCredit` varchar(20) DEFAULT NULL, " \
                                  "`newElegibleForFederalTaxCredit` varchar(20) DEFAULT NULL, " \
                                  "`UpdateDate` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, " \
                                  "PRIMARY KEY (`id`) " \
                                  ") ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 MAX_ROWS=4294967295; "
            creation_updated_report.execute(create_report_table_updated)

        with conn.cursor() as creation_obsoletes_report:
            create_report_table = "CREATE TABLE `AHRI_OBSOLETES_REPORT` ( " \
                                  "`id` int(11) unsigned NOT NULL AUTO_INCREMENT, " \
                                  "`AhriCertifiedRef` int(11) DEFAULT '0', " \
                                  "`OldAhriCertifiedRef` int(11) DEFAULT NULL, " \
                                  "`SystemType` int(11) DEFAULT '0', " \
                                  "`ModelStatus` int(11) DEFAULT '0', " \
                                  "`BrandName` longtext, " \
                                  "`OutdoorUnitModel` varchar(80) DEFAULT NULL, " \
                                  "`IndoorUnitModel` varchar(80) DEFAULT NULL, " \
                                  "`FurnaceModel` varchar(80) DEFAULT NULL, " \
                                  "`Capacity` int(11) DEFAULT '0', " \
                                  "`EER` decimal(7,4) DEFAULT '0.0000', " \
                                  "`SEER` decimal(7,4) DEFAULT '0.0000', " \
                                  "`HSPF` decimal(7,4) DEFAULT '0.0000', " \
                                  "`IEER` decimal(7,4) DEFAULT '0.0000', " \
                                  "`AHRIType` varchar(80) DEFAULT NULL, " \
                                  "`AFUE` decimal(8,4) DEFAULT '0.0000', " \
                                  "PRIMARY KEY (`id`) " \
                                  ") ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 MAX_ROWS=4294967295; "
            creation_obsoletes_report.execute(create_report_table)

        with conn.cursor() as create_ahri_all_tmp:
            create_tmp_table_all = "CREATE TABLE `AHRI_ALL_TMP` ( " \
                                    "`id` int(11) unsigned NOT NULL AUTO_INCREMENT, " \
                                    "`AhriCertifiedRef` int(11) DEFAULT '0', " \
                                    "`OldAhriCertifiedRef` int(11) DEFAULT NULL, " \
                                    "`SystemType` int(11) DEFAULT '0', " \
                                    "`ModelStatus` int(11) DEFAULT '0', " \
                                    "`ManufacturedType` varchar(255) DEFAULT NULL, " \
                                    "`BrandName` varchar(275) DEFAULT NULL, " \
                                    "`SeriesName` varchar(255) DEFAULT NULL, " \
                                    "`OutdoorUnitManufactured` varchar(255) DEFAULT NULL, " \
                                    "`OutdoorUnitModel` varchar(80) DEFAULT NULL, " \
                                    "`IndoorType` varchar(70) DEFAULT NULL, " \
                                    "`IndoorUnitManufactured` varchar(255) DEFAULT NULL, " \
                                    "`IndoorUnitModel` varchar(80) DEFAULT NULL, " \
                                    "`Refrigerant` varchar(80) DEFAULT NULL, " \
                                    "`IndoorAirQuantity` int(11) DEFAULT '0', " \
                                    "`IndoorAirQuantity2` int(11) DEFAULT '0', " \
                                    "`IndoorAirQuantity3` int(11) DEFAULT '0', " \
                                    "`FurnaceModel` varchar(80) DEFAULT NULL, " \
                                    "`FurnaceManufactured` varchar(255) DEFAULT NULL, " \
                                    "`Capacity` int(11) DEFAULT '0', " \
                                    "`EER` decimal(7,4) DEFAULT '0.0000', " \
                                    "`SEER` decimal(7,4) DEFAULT '0.0000', " \
                                    "`HighHeatingCapacity` int(11) DEFAULT '0', " \
                                    "`HighHeatingCapacity_COP` int(11) DEFAULT NULL, " \
                                    "`HSPF` decimal(7,4) DEFAULT '0.0000', " \
                                    "`LowHeatingCapacity` int(11) DEFAULT '0', " \
                                    "`LowHeatingCapacity_COP` int(11) DEFAULT NULL, " \
                                    "`IEER` decimal(7,4) DEFAULT '0.0000', " \
                                    "`Phase` int(11) DEFAULT '0', " \
                                    "`AHRIType` varchar(80) DEFAULT NULL, " \
                                    "`HSVTC` varchar(3) DEFAULT NULL, " \
                                    "`ExclusiveForExport` varchar(3) DEFAULT NULL, " \
                                    "`CoolingCost` int(11) DEFAULT '0', " \
                                    "`HeatingCost` int(11) DEFAULT '0', " \
                                    "`EstNationalCoolingCost` int(11) DEFAULT '0', " \
                                    "`ModelNumber` varchar(80) DEFAULT NULL, " \
                                    "`ManufacturedHousing` varchar(255) DEFAULT NULL, " \
                                    "`FuelType` varchar(80) DEFAULT NULL, " \
                                    "`Configuration` varchar(80) DEFAULT NULL, " \
                                    "`FurnaceType` varchar(80) DEFAULT NULL, " \
                                    "`InputRating` int(11) DEFAULT '0', " \
                                    "`OutputHeatingCapacity` int(11) DEFAULT '0', " \
                                    "`AFUE` decimal(8,4) DEFAULT '0.0000', " \
                                    "`Ef` decimal(8,4) DEFAULT '0.0000', " \
                                    "`Eae` int(11) DEFAULT '0', " \
                                    "`PE` int(11) DEFAULT '0', " \
                                    "`BasicModel` varchar(3) DEFAULT NULL, " \
                                    "`ElectricallyCommutatedMotor` varchar(20) DEFAULT NULL, " \
                                    "`ElegibleForFederalTaxCredit` varchar(20) DEFAULT NULL, " \
                                    "`UpdateDate` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, " \
                                    "`AHRITypeDescription` varchar(255) DEFAULT NULL, " \
                                    "PRIMARY KEY (`id`), " \
                                    "KEY `AHRIREF_IDX` (`AhriCertifiedRef`), "\
                                    "KEY `FURNACE_IDX` (`FurnaceModel`) USING BTREE, "\
                                    "KEY `COMBO1_IDX` (`OutdoorUnitModel`,`IndoorUnitModel`,`FurnaceModel`) USING BTREE, "\
                                    "KEY `COMBO4_IDX` (`IndoorUnitModel`,`FurnaceModel`) USING BTREE, "\
                                    "KEY `IDX_Sys_Furnace_Status` (`SystemType`,`FurnaceModel`,`ModelStatus`) USING BTREE, "\
                                    "KEY `IDX_AHRI_SEER_Cap_Brand` (`AHRIType`,`SEER`,`Capacity`,`BrandName`) USING BTREE, "\
                                    "KEY `COMBO3_IDX` (`OutdoorUnitModel`,`ManufacturedType`,`FurnaceModel`) USING BTREE "\
                                    ") ENGINE=InnoDB AUTO_INCREMENT=9733947 DEFAULT CHARSET=utf8 MAX_ROWS=4294967295;"
            
            create_ahri_all_tmp.execute(create_tmp_table_all)
        
        with conn.cursor() as create_bl_temp:
            create_boilers_tmp = "CREATE TABLE `AHRI_BOILERS_TMP` ( " \
                                    "`id` int(11) unsigned NOT NULL AUTO_INCREMENT, " \
                                    "`AhriCertifiedRef` int(11) DEFAULT NULL, " \
                                    "`OldAhriCertifiedRef` int(11) DEFAULT NULL, " \
                                    "`ModelStatus` int(11) DEFAULT NULL, " \
                                    "`BrandName` varchar(255) DEFAULT NULL, " \
                                    "`ManufacturedType` varchar(255) DEFAULT NULL, " \
                                    "`ModelNumber` varchar(80) DEFAULT NULL, " \
                                    "`Material` varchar(255) DEFAULT NULL, " \
                                    "`Location` varchar(255) DEFAULT NULL, " \
                                    "`FuelType` varchar(80) DEFAULT NULL, " \
                                    "`HeatingMedium` varchar(80) DEFAULT NULL, " \
                                    "`InputMBTUH` int(11) DEFAULT NULL, " \
                                    "`GrossOutputMBTUH` int(11) DEFAULT NULL, " \
                                    "`CombustionEfficiency` double DEFAULT NULL, " \
                                    "`ThermalEfficiency` double DEFAULT NULL, " \
                                    "`HeatingCapacity` double DEFAULT NULL, " \
                                    "`Afue` double DEFAULT NULL, " \
                                    "`Ef` double DEFAULT NULL, " \
                                    "`Eae` int(11) DEFAULT NULL, " \
                                    "`Pe` int(11) DEFAULT NULL, " \
                                    "`RatingsSteam` double DEFAULT NULL, " \
                                    "`RatingsWater` double DEFAULT NULL, " \
                                    "`IgnitionType` varchar(80) DEFAULT NULL, " \
                                    "`DraftType` varchar(80) DEFAULT NULL, " \
                                    "`ElegibleForFederalTaxes` varchar(80) DEFAULT NULL, " \
                                    "`Control` varchar(80) DEFAULT NULL, " \
                                    "`SoldUsa` varchar(80) DEFAULT NULL, " \
                                    "`CO2` double DEFAULT NULL, " \
                                    "`UpdateDate` timestamp NULL DEFAULT NULL, " \
                                    "PRIMARY KEY (`id`) " \
                                    ") ENGINE=InnoDB DEFAULT CHARSET=utf8;"
                                    
            create_bl_temp.execute(create_boilers_tmp)

        conn.commit()
        conn.close()
        self.logger.info("Enterprise support created Sucessfully")

    def removeReportsTable(self):
        conn = self.connectToDatabase('Enterprise')
        with conn.cursor() as delete_tmp_reports:
            deletes_query = "DROP TABLE IF EXISTS AHRI_ALL_TMP, AHRI_BOILERS_TMP, AHRI_OBSOLETES_REPORT, AHRI_INSERTION_REPORT, AHRI_UPDATED_REPORT;"
            delete_tmp_reports.execute(deletes_query)
        conn.commit()
        conn.close()
