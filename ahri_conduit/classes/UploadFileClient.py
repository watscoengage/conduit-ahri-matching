import os
import boto
from boto.s3.key import Key
import boto3
import botocore
import logging

class UploadFileClient :

    def __init__(self, awsAccessKey, awsSecretAccessKey):
        self.awsAccessKey = awsAccessKey
        self.awsSecretAccessKey = awsSecretAccessKey

    def uploadFileToS3(self, file, bucket, key, callback=None, md5=None, reduced_redundancy=False, content_type=None):
        try:
            size = os.fstat(file.fileno()).st_size
        except:
            file.seek(0, os.SEEK_END)
            size = file.tell()

        conn = boto.connect_s3(self.awsAccessKey, self.awsSecretAccessKey)
        bucket = conn.get_bucket(bucket, validate=True)
        k = Key(bucket)
        k.key = key
        if content_type:
            k.set_metadata('Content-Type', content_type)
        sent = k.set_contents_from_file(file, cb=callback, md5=md5, reduced_redundancy=reduced_redundancy, rewind=True)

        file.seek(0)

        if sent == size:
            return True
        return False

class DownloadFileClient :

    def __init__(self, awsAccessKey, awsSecretAccessKey):
            self.awsAccessKey = awsAccessKey
            self.awsSecretAccessKey = awsSecretAccessKey

    def downloadFilesFromS3(self, bucket, key_prefix, key_array, tmp_path):
        conn = boto.connect_s3(self.awsAccessKey, self.awsSecretAccessKey)

        bucket_object = conn.get_bucket(bucket, validate=True)
        for key_name in key_array:
            
            try:
                k = Key(bucket_object, key_prefix + '/' + key_name)
                k.get_contents_to_filename(tmp_path + key_name)
                
            except :
                logging.info(key_prefix + '/' + key_name + " doesn't exist")
                
                
            