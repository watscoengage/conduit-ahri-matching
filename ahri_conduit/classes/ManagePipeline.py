import pymysql
import boto3
import logging
import time
import json
import uuid
import constant as constant

class ManagePipeline:
    def __init__(self, databaseEndpoint, databaseUser, databasePassword, databaseName, pipelineDictPath):
        self.dataOrchestration = boto3.client('datapipeline')
        self.databaseEndpoint = databaseEndpoint
        self.databaseUser = databaseUser
        self.databasePassword = databasePassword
        self.databaseName = databaseName
        self.logger = logging.getLogger()
        self.maker = ''
        self.pipelineDictFilePath = pipelineDictPath
        self.pipelineDict = self.readingJsonFile(self.pipelineDictFilePath + 'PipelinesListDict.json')

        self.logger.info('Database endpoint: ' + databaseEndpoint)
        self.logger.info('Database user: ' + databaseUser)
        self.logger.info('Database pass: ' + databasePassword)
        self.logger.info('Database name: ' + databaseName)


    def startPipeline(self, id, fileDefPath="none"):
        pipelineJsonObj = self.fineJsonValue(self.pipelineDict, 'id', id, 'value')
        pipelineId = pipelineJsonObj['pipelineId']
        self.logger.info('pipelineId: ' + pipelineId)

        if self.checkIfPipelineExist(pipelineId):
            self.activatePipeline(pipelineJsonObj)
            return pipelineId
        elif fileDefPath != "none":
            pipelineId = self.createPipeline(id, fileDefPath, pipelineJsonObj)
            return pipelineId
        else:
            self.logger.info('start pipeline with id:' + id + 'not found.')
            return ''

    def checkIfPipelineExist(self, pipelineId):
        if pipelineId == '':
            return False
        response = self.dataOrchestration.list_pipelines(marker=self.maker)
        self.logger.info('pipelineList: ' + str(response))

        if 'pipelineIdList' in response:
            jsonValue = self.fineJsonValue(response['pipelineIdList'], 'id', pipelineId, 'name')
            return jsonValue != ''
        else:
            if 'maker' in response and 'hasMoreResults' in response and response['hasMoreResults'] == 'True':
                self.maker = response['maker']
                self.checkIfPipelineExist(pipelineId)
            else:
                return False


    def activatePipeline(self, pipelineObj):
        pipelineId = pipelineObj['pipelineId']
        definitionPath = self.getDefinitionByPipelineName(pipelineObj['pipelineName'])
        pipelineJsonValues = self.readingJsonFile(definitionPath)
        values = self.definition_to_parameter_values(pipelineJsonValues)
        self.logger.info('Pipelines Values: ' + str(values))

        valuesObj = self.setPipelineParametersValues(values, pipelineObj)

        try:
            self.dataOrchestration.activate_pipeline(
                pipelineId=pipelineId,
                parameterValues=valuesObj,
                startTimestamp=time.time()
            )
            self.logger.info('pipeline with id: ' + pipelineId + ' activated')
        except Exception as e:
            self.logger.error(str(e))
            return ''

    def createPipeline(self, id, fileDefPath, pipelineJsonObj):
        #build pipeline definition
        definition = self.buildingPipelineDefinition(fileDefPath, pipelineJsonObj)
        self.logger.info('definition: ' + str(definition))

        #adding the pipeline
        pipeline = self.dataOrchestration.create_pipeline(name=id, uniqueId=str(uuid.uuid1()))
        self.logger.info('pipeline: ' + str(pipeline) + 'created')

        #setting definition
        if 'pipelineId' in pipeline:
            pipelineId = pipeline['pipelineId']

            response = self.putPipelineDefinition(pipelineId, definition)
            self.logger.info('pipeline definition response: ' + str(response))

            #updating pipeline id on the dictionary
            for pipelineDict in self.pipelineDict:
                if pipelineDict['id'] == id:
                    pipelineDict['value']['pipelineId'] = pipelineId
            self.writingJsonFile(self.pipelineDictFilePath + 'PipelinesListDict.json', self.pipelineDict)
            return pipelineId
        else:
            return ''

    def buildingPipelineDefinition(self, fileDefPath, pipelineObj):
        pipelineJsonDef = self.readingJsonFile(fileDefPath)
        # objects definitions
        objects = self.definition_to_api_objects(pipelineJsonDef)
        # self.logger.info('OBJECTS')
        # self.logger.info(objects)
        # parameters definitions

        parameters = self.definition_to_api_parameters(pipelineJsonDef)
        # self.logger.info('PARAMETERS')
        # self.logger.info(parameters)

        # values definition
        values = self.definition_to_parameter_values(pipelineJsonDef)
        # self.logger.info('VALUES')
        # self.logger.info(values)
        valuesObj = self.setPipelineParametersValues(values, pipelineObj)
        return {
            'objects': objects,
            'parameters': parameters,
            'values': valuesObj
        }


    def putPipelineDefinition(self, pipelineId, definition):
        try:
            response = self.dataOrchestration.put_pipeline_definition(
                pipelineId=pipelineId,
                pipelineObjects=definition['objects'],
                parameterObjects=definition['parameters'],
                parameterValues=definition['values']
            )
            return response
        except Exception as e:
            self.logger.error(str(e))
            return ''


    def StartPipelineProcess(self, pipelineId):
        self.dataOrchestration.activate_pipeline(
            pipelineId=pipelineId,
            startTimestamp=time.time()
        )

    def DescribePipelineProcess(self, pipelineId):
        pipeline_status = self.dataOrchestration.describe_pipelines(pipelineIds=[pipelineId])
        return pipeline_status

    def CheckPipelineExecution(self, pipelineId):
        # This function will check if pipeline is finished.
        pipeline_state = 'UNKNOW'
        desired_pipeline_state = 'FINISHED'
        while pipeline_state != desired_pipeline_state:
            # make a request every two minutes
            time.sleep(120)
            pipeline_description = self.DescribePipelineProcess(pipelineId)
            for key_value in pipeline_description['pipelineDescriptionList'][0]['fields']:
                if key_value['key'] == '@pipelineState':
                    pipeline_state = key_value['stringValue']
                    break
            self.logger.info('Pipeline ' + pipelineId + ' state:' + pipeline_state)

    def readingJsonFile(self, filename):
        # Read JSON data into the datastore variable
        with open(filename) as json_file:
            json_data = json.load(json_file)
            return json_data

    def writingJsonFile(self, filename, data):
        with open(filename, "w") as jsonFile:
            json.dump(data, jsonFile)

    def fineJsonValue(self, json_object, prop, value, returnValue):
        for dict in json_object:
            if dict[prop] == value:
                return dict[returnValue]

    def definition_to_api_objects(self, definition):
        try:
            if 'objects' not in definition:
                self.logger.info('Missing "objects" key', definition)
            api_elements = []
            # To convert to the structure expected by the service,
            # we convert the existing structure to a list of dictionaries.
            # Each dictionary has a 'fields', 'id', and 'name' key.
            for element in definition['objects']:
                try:
                    element_id = element['id']
                except KeyError:
                    self.logger.error('Missing "id" key of element: %s' % json.dumps(element), definition)
                api_object = {'id': element_id}
                # If a name is provided, then we use that for the name,
                # otherwise the id is used for the name.
                try:
                    name = element['name']
                except KeyError:
                    name = element_id
                api_object['name'] = name
                # Now we need the field list.  Each element in the field list is a dict
                # with a 'key', 'stringValue'|'refValue'
                fields = []
                for key, value in element.items():
                    if value != element_id:
                        fields.extend(self._parse_each_field(key, value))
                api_object['fields'] = fields
                api_elements.append(api_object)
            return api_elements
        except Exception as e:
            self.logger.error(str(e))
            return ''

    def definition_to_api_parameters(self, definition):
        if 'parameters' not in definition:
            return None
        parameter_objects = []
        for element in definition['parameters']:
            try:
                parameter_id = element['id']
            except KeyError:
                self.logger.error('Missing "id" key of parameter: %s' % json.dumps(element), definition)
            parameter_object = {'id': parameter_id}
            # Now we need the attribute list.  Each element in the attribute list
            # is a dict with a 'key', 'stringValue'
            attributes = []
            for key, value in element.items():
                if value != parameter_id:
                    attributes.extend(self._parse_each_field(key, value))
            parameter_object['attributes'] = attributes
            parameter_objects.append(parameter_object)
        return parameter_objects

    def definition_to_parameter_values(self, definition):
        if 'values' not in definition:
            return None
        parameter_values = []
        for key in definition['values']:
            parameter_values.extend(
                self._convert_single_parameter_value(key, definition['values'][key]))

        return parameter_values

    def _parse_each_field(self, key, value):
        values = []
        if isinstance(value, list):
            for item in value:
                values.append(self._convert_single_field(key, item))
        else:
            values.append(self._convert_single_field(key, value))
        return values

    def _convert_single_field(self, key, value):
        field = {'key': key}
        if isinstance(value, dict) and list(value.keys()) == ['ref']:
            field['refValue'] = value['ref']
        else:
            field['stringValue'] = value
        return field

    def _convert_single_parameter_value(self, key, values):
        parameter_values = []
        if isinstance(values, list):
            for each_value in values:
                parameter_value = {'id': key, 'stringValue': each_value}
                parameter_values.append(parameter_value)
        else:
            parameter_value = {'id': key, 'stringValue': values}
            parameter_values.append(parameter_value)
        return parameter_values

    def setPipelineParametersValues(self, valuesObj, pipelineObj):
        for value in valuesObj:
            if value['id'] == 'myRDSConnectStr':
                connection = 'jdbc:mysql://' + self.databaseEndpoint + ':3306/' + self.databaseName + '?jdbcCompliantTruncation=false'
                value['stringValue'] = value['stringValue'].replace('{{ENDPOINT}}', connection)
            if value['id'] == 'myRDSUsername':
                value['stringValue'] = self.databaseUser
            if value['id'] == '*myRDSPassword':
                value['stringValue'] = self.databasePassword
            if value['id'] == 'myRDSTableInsertSql':
                value['stringValue'] = pipelineObj['myRDSTableInsertSql']
            if value['id'] == 'myRDSTableName':
                value['stringValue'] = pipelineObj['myRDSTableName']
            if value['id'] == 'myInputS3Loc':
                if '{{OCA_MATCHES_FOLDER}}' in pipelineObj['myInputS3Loc']:
                    value['stringValue'] = pipelineObj['myInputS3Loc'].replace('{{OCA_MATCHES_FOLDER}}', constant.OCA_MATCHES_FOLDER)
                else:
                    value['stringValue'] = pipelineObj['myInputS3Loc']
            if value['id'] == 'myRDSInstanceId':
                value['stringValue'] = self.databaseEndpoint
        self.logger.info('Pipelines Values with information: ' + str(valuesObj))
        return valuesObj

    def getDefinitionByPipelineName(self, pipelineName):
        if 'OCA' in pipelineName:
            return self.pipelineDictFilePath + "PipelineInsertPIMMatchesDefinition.json"
        elif pipelineName == 'CONDUIT_UPGRADE_GENERAL_MODEL':
            return self.pipelineDictFilePath + "UpgrateGeneralModelsDefinition.json"
        elif 'CONDUIT' in pipelineName:
            return self.pipelineDictFilePath + "InsertFromCSVToTableDefinition.json"




