import pymysql
import logging
import sys
import HTML
from datetime import datetime
import time
import zipfile
import zlib
import os
from zipfile import ZipFile
from UploadFileClient import UploadFileClient
import constant as constant

class ConduitReportWrap:
    def __init__(self, databaseEndpoint, databaseUser, databasePassword, databaseName, logPath):
        self.databaseEndpoint = databaseEndpoint
        self.databaseUser = databaseUser
        self.databasePassword = databasePassword
        self.databaseName = databaseName
        self.logPath = logPath

    def connectToDatabase(self):
        # rds settings to connect to the database
        rds_host = self.databaseEndpoint
        name = self.databaseUser
        password = self.databasePassword
        db_name = self.databaseName

        logger = logging.getLogger()
        try:
            conn = pymysql.connect(rds_host, user=name, passwd=password, db=db_name, connect_timeout=5)
            logger.info("SUCCESS: Connection to RDS mysql instance succeeded")
            return conn
        except:
            logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
            sys.exit()

    def createHTMLReportsConduit(self):
        conn = self.connectToDatabase()

        inserted_report_file = self.logPath + 'ahri_inserted_report.csv'
        modified_report_file = self.logPath + 'ahri_modified_report.csv'
        obsoletes_report_file = self.logPath + 'ahri_obsoletes_report.csv'
        product_report_file = self.logPath + 'ahri_product_report.html'
        inserted_report_handler = open(inserted_report_file, 'a+')
        modified_report_handler = open(modified_report_file, 'a+')
        obsoletes_report_handler = open(obsoletes_report_file, 'a+')
        product_report_handler = open(product_report_file, 'a+')

        with conn.cursor() as create_product_report:
            count_inserted = "SELECT COUNT(*) FROM AHRI_INSERTION_REPORT;"
            count_obsoletes = "SELECT COUNT(*) FROM AHRI_OBSOLETES_REPORT;"
            count_modified = "SELECT COUNT(*) FROM AHRI_UPDATED_REPORT;"

            Inserted_By_SystemType = "SELECT COUNT(*), SystemType FROM AHRI_INSERTION_REPORT Group By SystemType;"
            create_product_report.execute(Inserted_By_SystemType)
            rowsinserted = create_product_report.fetchall()

            modified_By_SystemType = "SELECT COUNT(*), SystemType FROM AHRI_UPDATED_REPORT Group By SystemType;"
            create_product_report.execute(modified_By_SystemType)
            rowsmodified = create_product_report.fetchall()

            Obsoletes_By_SystemType = "SELECT COUNT(*), SystemType FROM AHRI_OBSOLETES_REPORT Group By SystemType;"
            create_product_report.execute(Obsoletes_By_SystemType)
            rowsobsoletes = create_product_report.fetchall()

            #1 => Straight Cool
            #2 => Heat Pump
            #3 => Furnace
            #4 => ULE

            straight_cool_data = ['Air Conditioner and Air Conditioner Coils',' - ',' - ',' - ']
            heat_pump_data = ['Heat Pumps and Heat Pump Coils',' - ',' - ',' - ']
            furnace_data = ['Furnaces',' - ',' - ',' - ']
            ule_data = ['Unitary Large Equipment',' - ',' - ',' - ']

            system_type_data = []

            self.create_data_grid(straight_cool_data, heat_pump_data, furnace_data, ule_data, rowsinserted, 1)
            self.create_data_grid(straight_cool_data, heat_pump_data, furnace_data, ule_data, rowsmodified, 2)
            self.create_data_grid(straight_cool_data, heat_pump_data, furnace_data, ule_data, rowsobsoletes, 3)

            system_type_data.append(straight_cool_data)
            system_type_data.append(heat_pump_data)
            system_type_data.append(furnace_data)
            system_type_data.append(ule_data)

            inserted_count = ''
            create_product_report.execute(count_inserted)
            for row in create_product_report:
                inserted_count = row[0]

            deleted_count = ''
            create_product_report.execute(count_obsoletes)
            for rowobsolete in create_product_report:
                deleted_count = rowobsolete[0]

            modified_count = ''
            create_product_report.execute(count_modified)
            for rowmodified in create_product_report:
                modified_count = rowmodified[0]

            system_type_data.append(['TOTAL', inserted_count, modified_count, deleted_count ])


            conduit_report = HTML.table(system_type_data, header_row=['System Type', 'Inserted','Modified','Obsoletes'])
            product_report_handler.write("</BR>" + conduit_report + "</BR>")
            product_report_handler.close()


        with conn.cursor() as read_insert_reports:
            read_insert_table = "SELECT AhriCertifiedRef, SystemType, ModelStatus, BrandName, OutdoorUnitModel, IndoorUnitModel, FurnaceModel, Capacity, EER, SEER, HSPF, IEER, AHRIType, AFUE FROM AHRI_INSERTION_REPORT"
            read_insert_reports.execute(read_insert_table)
            rows = read_insert_reports.fetchall()

            inserted_report_handler.write('AhriCertifiedRef,SystemType,ModelStatus,BrandName,OutdoorUnitModel,IndoorUnitModel,FurnaceModel,Capacity,EER,SEER,HSPF,IEER,AHRIType,AFUE\n')

            for elements in rows:
                inserted_report_handler.write(
                    str(elements[0])+','+str(elements[1])+','+str(elements[2])+','+str(elements[3])+','+str(elements[4])+','+str(elements[5])+','+str(elements[6])+','+str(elements[7])+','+str(elements[8])+','+str(elements[9])+','+str(elements[10])+','+str(elements[11])+','+str(elements[12])+','+str(elements[13])+'\n')
            inserted_report_handler.close()

        with conn.cursor() as read_modified_reports:
            read_modified_table = "SELECT AhriCertifiedRef,SystemType,ModelStatus,newModelStatus,ManufacturedType,newManufacturedType,BrandName,newBrandName,SeriesName,newSeriesName,OutdoorUnitManufactured,newOutdoorUnitManufactured,OutdoorUnitModel,newOutdoorUnitModel,IndoorType,newIndoorType,IndoorUnitManufactured,newIndoorUnitManufactured,IndoorUnitModel,newIndoorUnitModel,Refrigerant,newRefrigerant,IndoorAirQuantity,newIndoorAirQuantity,IndoorAirQuantity2,newIndoorAirQuantity2,IndoorAirQuantity3,newIndoorAirQuantity3,FurnaceModel,newFurnaceModel,FurnaceManufactured,newFurnaceManufactured,Capacity,newCapacity,EER,newEER,SEER,newSEER,HighHeatingCapacity,newHighHeatingCapacity,HighHeatingCapacity_COP,newHighHeatingCapacity_COP,HSPF,newHSPF,LowHeatingCapacity,newLowHeatingCapacity,LowHeatingCapacity_COP,newLowHeatingCapacity_COP,IEER,newIEER,Phase,newPhase,AHRIType,newAHRIType,HSVTC,newHSVTC,ExclusiveForExport,newExclusiveForExport,CoolingCost,newCoolingCost,HeatingCost,newHeatingCost,EstNationalCoolingCost,newEstNationalCoolingCost,ModelNumber,newModelNumber,ManufacturedHousing,newManufacturedHousing,FuelType,newFuelType,Configuration,newConfiguration,FurnaceType,newFurnaceType,InputRating,newInputRating,OutputHeatingCapacity,newOutputHeatingCapacity,AFUE,newAFUE,Ef,newEf,Eae,newEae,PE,newPE,BasicModel,newBasicModel,ElectricallyCommutatedMotor,newElectricallyCommutatedMotor,ElegibleForFederalTaxCredit,newElegibleForFederalTaxCredit FROM AHRI_UPDATED_REPORT"
            read_modified_reports.execute(read_modified_table)
            rows = read_modified_reports.fetchall()

            modified_report_handler.write('AhriCertifiedRef,SystemType,ModelStatus,newModelStatus,ManufacturedType,newManufacturedType,BrandName,newBrandName,SeriesName,newSeriesName,OutdoorUnitManufactured,newOutdoorUnitManufactured,OutdoorUnitModel,newOutdoorUnitModel,IndoorType,newIndoorType,IndoorUnitManufactured,newIndoorUnitManufactured,IndoorUnitModel,newIndoorUnitModel,Refrigerant,newRefrigerant,IndoorAirQuantity,newIndoorAirQuantity,IndoorAirQuantity2,newIndoorAirQuantity2,IndoorAirQuantity3,newIndoorAirQuantity3,FurnaceModel,newFurnaceModel,FurnaceManufactured,newFurnaceManufactured,Capacity,newCapacity,EER,newEER,SEER,newSEER,HighHeatingCapacity,newHighHeatingCapacity,HighHeatingCapacity_COP,newHighHeatingCapacity_COP,HSPF,newHSPF,LowHeatingCapacity,newLowHeatingCapacity,LowHeatingCapacity_COP,newLowHeatingCapacity_COP,IEER,newIEER,Phase,newPhase,AHRIType,newAHRIType,HSVTC,newHSVTC,ExclusiveForExport,newExclusiveForExport,CoolingCost,newCoolingCost,HeatingCost,newHeatingCost,EstNationalCoolingCost,newEstNationalCoolingCost,ModelNumber,newModelNumber,ManufacturedHousing,newManufacturedHousing,FuelType,newFuelType,Configuration,newConfiguration,FurnaceType,newFurnaceType,InputRating,newInputRating,OutputHeatingCapacity,newOutputHeatingCapacity,AFUE,newAFUE,Ef,newEf,Eae,newEae,PE,newPE,BasicModel,newBasicModel,ElectricallyCommutatedMotor,newElectricallyCommutatedMotor,ElegibleForFederalTaxCredit,newElegibleForFederalTaxCredit\n')

            for elements in rows:
                count = 0
                line = ''
                while count <= 91:
                    line += str(elements[count])+','
                    if count == 91:
                        line += str(elements[count])
                    count += 1

                modified_report_handler.write(line+'\n')
            modified_report_handler.close()

        with conn.cursor() as read_obsoletes_reports:
            read_obsoletes_table = "SELECT AhriCertifiedRef, SystemType, ModelStatus, BrandName, OutdoorUnitModel, IndoorUnitModel, FurnaceModel, Capacity, EER, SEER, HSPF, IEER, AHRIType, AFUE FROM AHRI_OBSOLETES_REPORT"
            read_obsoletes_reports.execute(read_obsoletes_table)
            rows = read_obsoletes_reports.fetchall()

            obsoletes_report_handler.write(
                'AhriCertifiedRef,SystemType,ModelStatus,BrandName,OutdoorUnitModel,IndoorUnitModel,FurnaceModel,Capacity,EER,SEER,HSPF,IEER,AHRIType,AFUE\n')

            for elements in rows:
                obsoletes_report_handler.write(
                    str(elements[0]) + ',' + str(elements[1]) + ',' + str(elements[2]) + ',' + str(
                        elements[3]) + ',' + str(elements[4]) + ',' + str(elements[5]) + ',' + str(
                        elements[6]) + ',' + str(elements[7]) + ',' + str(elements[8]) + ',' + str(
                        elements[9]) + ',' + str(elements[10]) + ',' + str(elements[11]) + ',' + str(
                        elements[12]) + ',' + str(elements[13])+'\n')
            obsoletes_report_handler.close()

        conn.close()
        self.zip_compress_detailed_reports()

    def create_data_grid(self, straight_cool_data, heat_pump_data, furnace_data, ule_data, data_files, index):
        for data in data_files:
            inserted = data[0]
            systemtype = data[1]
            if systemtype == 1:
                straight_cool_data[index] = inserted
            elif systemtype == 2:
                heat_pump_data[index] = inserted
            elif systemtype == 3:
                furnace_data[index] = inserted
            elif systemtype == 4:
                ule_data[index] = inserted

    def zip_compress_detailed_reports(self):
        logger = logging.getLogger()
        try:
            compression = zipfile.ZIP_DEFLATED
        except:
            compression = zipfile.ZIP_STORED

        modes = {zipfile.ZIP_DEFLATED: 'deflated',
                 zipfile.ZIP_STORED: 'stored',
                 }

        files = [
            'ahri_inserted_report.csv',
            'ahri_obsoletes_report.csv',
            'ahri_modified_report.csv'
        ]

        for file in files:
            logger.info('creating archive')
            zf = zipfile.ZipFile(self.logPath + file[:-4] + '.zip', mode='w')
            try:
                zf.write(self.logPath + file, file, compress_type=compression)
            finally:
                logger.info('closing')
                zf.close()

    def removeFileContent(self):
        # remove the content after reading it
        open(self.logPath + 'ahri_product_report.html', 'w').close()

    def read_conduit_report_files(self):
        logPath = self.logPath
        fileNames = [
            'ahri_product_report.html'
        ]

        contentFile = ''
        for fileName in fileNames:
            if os.path.exists(logPath + fileName):
                file = open(logPath + fileName, 'r+')
                contentFile += file.read()
                contentFile += "<BR><BR>"
            else:
                logger = logging.getLogger()
                logger.error("ERROR: File does not exist")
        return contentFile

    def upload_file_to_s3(self, awsAccessKeyId, awsSecretAccessKey, ahriFilePath, logPath, bucket, bucketArchive):
        logger = logging.getLogger()
        uploadFileClient = UploadFileClient(awsAccessKeyId, awsSecretAccessKey)
        uploadfiles = 0
        zipmode = 'w'
        for file in os.listdir(ahriFilePath):
            if file.endswith(".zip"):
                fileHandler = open(ahriFilePath + file, 'r')
                with ZipFile(ahriFilePath + 'AHRI_Files_' + time.strftime("%m%d%Y") + '.zip', zipmode) as ahri_zip:
                    zipmode = 'a'
                    ahri_zip.write(ahriFilePath + file, file)
                    ahri_zip.close()

                key = "ahrizipfiles/" + file

                if uploadFileClient.uploadFileToS3(fileHandler, bucket, key):
                    logger.info(key + ' uploaded')
                    uploadfiles += 1
                else:
                    logger.info('The upload of ' + key + ' file failed')

        zipmode = 'a'
        for file in os.listdir(logPath):
            if file.endswith(".zip"):
                with ZipFile(ahriFilePath + 'AHRI_Files_' + time.strftime("%m%d%Y") + '.zip', zipmode) as ahri_zip:
                    ahri_zip.write(logPath + file, file)
                    ahri_zip.close()

        zipHandler = open(ahriFilePath + 'AHRI_Files_' + time.strftime("%m%d%Y") + '.zip', 'r')
        if uploadFileClient.uploadFileToS3(zipHandler, bucketArchive, 'AHRI_Files_' + time.strftime("%m%d%Y") + '.zip'):
            logger.info('file uploaded to the archive')
        else:
            logger.info('The upload of ' + key + ' file failed to archive')