import pymysql
import logging
import sys
import os
import csv

class FilesACMaps:
    def __init__(self, ac_path, vsmsac_path,ac_obsolete_path, vsmsac_obsolete_path):
        self.new_ac_file_path = ac_path
        self.new_vsmsac_path = vsmsac_path
        self.new_ac_obsolete_file_path = ac_obsolete_path
        self.new_vsmsac_obsolete_path = vsmsac_obsolete_path
        logging.info(self.new_ac_file_path)
        logging.info(self.new_vsmsac_path)
        logging.info(self.new_ac_obsolete_file_path)
        logging.info(self.new_vsmsac_obsolete_path)
        logging.info(os.stat(self.new_ac_file_path).st_size)


    def mapACFile(self):
        rownum = 0
        headers = []
        proc_file_name = self.new_ac_file_path.replace('AC','AC_MAP')
        proc_file_name = proc_file_name.replace('Active','')
        logging.info(proc_file_name)

        if os.path.isfile(self.new_ac_file_path):
            if os.stat(self.new_ac_file_path).st_size == 0:
                pass
            else:
                with open(self.new_ac_file_path, 'rb') as f:
                    with open(proc_file_name, 'a') as w:
                        writer = csv.writer(w, quoting=csv.QUOTE_ALL)
                        reader = csv.reader(f)
                        for row in reader:
                            if rownum == 0:
                                headers = row
                            else:
                                writer.writerow(self.mappingACNewContentFiles(row, headers))
                            rownum += 1

    def mapACOBSFile(self):
        rownum = 0
        headers = []
        proc_file_name = self.new_ac_obsolete_file_path.replace('AC','AC_MAP')
        proc_file_name = proc_file_name.replace('Obsolete','')
        logging.info(proc_file_name)

        if os.path.isfile(self.new_ac_obsolete_file_path):
            if os.stat(self.new_ac_obsolete_file_path).st_size == 0:
                pass
            else:
                with open(self.new_ac_obsolete_file_path, 'rb') as f:
                    with open(proc_file_name, 'a') as w:
                        writer = csv.writer(w, quoting=csv.QUOTE_ALL)
                        reader = csv.reader(f)
                        for row in reader:
                            if rownum == 0:
                                headers = row
                            else:
                                writer.writerow(self.mappingACNewContentFiles(row, headers))
                            rownum += 1

    def mapVSMSACFile(self):
        rownum = 0
        headers = []
        proc_file_name = self.new_vsmsac_path.replace('VSMSAC','VSMSAC_MAP')
        proc_file_name = proc_file_name.replace('Active','')
        logging.info(proc_file_name)        
        if os.path.isfile(self.new_vsmsac_path):
            if os.stat(self.new_vsmsac_path).st_size == 0:
                pass
            else:
                with open(self.new_vsmsac_path, 'rb') as f:            
                    with open(proc_file_name, 'a') as w:
                        writer = csv.writer(w, quoting=csv.QUOTE_ALL)
                        reader = csv.reader(f)
                        for row in reader:
                            if rownum == 0:
                                headers = row
                            else:
                                writer.writerow(self.mappingVSMSACContentFiles(row, headers))
                            rownum += 1

    def mapVSMSACOBSFile(self):
        rownum = 0
        headers = []
        proc_file_name = self.new_vsmsac_obsolete_path.replace('VSMSAC','VSMSAC_MAP')
        proc_file_name = proc_file_name.replace('Obsolete','')
        logging.info(proc_file_name)            
        if os.path.isfile(self.new_vsmsac_obsolete_path):
            if os.stat(self.new_vsmsac_obsolete_path).st_size == 0:
                pass
            else:
                with open(self.new_vsmsac_obsolete_path, 'rb') as f:            
                    with open(proc_file_name, 'a') as w:
                        writer = csv.writer(w, quoting=csv.QUOTE_ALL)
                        reader = csv.reader(f)
                        for row in reader:
                            if rownum == 0:
                                headers = row
                            else:
                                writer.writerow(self.mappingVSMSACContentFiles(row, headers))
                            rownum += 1                    

    def mappingACNewContentFiles(self, ac_content_array, headers):
        old_format_line_build = []
        old_format_line_build.append(ac_content_array[headers.index("AHRI Certified Reference Number")])
        old_format_line_build.append(ac_content_array[headers.index("Old AHRI Reference Number")])
        old_format_line_build.append(ac_content_array[headers.index("Model Status")])
        old_format_line_build.append(ac_content_array[headers.index("Manufacturer Type")])
        old_format_line_build.append(ac_content_array[headers.index("Outdoor Unit Brand Name")])
        old_format_line_build.append(ac_content_array[headers.index("Series Name")])
        old_format_line_build.append(ac_content_array[headers.index("Outdoor Unit Brand Name")])
        old_format_line_build.append(ac_content_array[headers.index("Outdoor Unit Model Number  (Condenser or Single Package)")])
        old_format_line_build.append(ac_content_array[headers.index("Indoor Unit Brand Name")])
        old_format_line_build.append(ac_content_array[headers.index("Indoor Unit Model Number (Evaporator and/or Air Handler)")])
        old_format_line_build.append(ac_content_array[headers.index("Indoor Full-Load Air Volume Rate (A2 SCFM)")])
        old_format_line_build.append(ac_content_array[headers.index("Indoor Cooling Intermediate Air Volume Rate (Ev SCFM)")])
        old_format_line_build.append(ac_content_array[headers.index("Indoor Cooling Minimum Air Volume Rate (B1 SCFM)")])
        old_format_line_build.append(ac_content_array[headers.index("Furnace Model Number")])
        old_format_line_build.append(ac_content_array[headers.index("Cooling Capacity (A2) - Single or High Stage (95F),btuh")])
        old_format_line_build.append(ac_content_array[headers.index("EER (A2) - Single or High Stage (95F)")])
        old_format_line_build.append(ac_content_array[headers.index("SEER")])
        old_format_line_build.append(ac_content_array[headers.index("IEER")])
        old_format_line_build.append(ac_content_array[headers.index("Phase")])
        if len(ac_content_array[headers.index("AHRI Type")]) == 0:
            old_format_line_build.append(ac_content_array[headers.index("AHRI Type")])
        else:
            x = ac_content_array[headers.index("AHRI Type")].split(" (",1)
            old_format_line_build.append(x[0])
        #old_format_line_build.append(ac_content_array[headers.index("AHRI Type")].split(" (",1))
        old_format_line_build.append(ac_content_array[headers.index("Designated Tested Combination.")])
        old_format_line_build.append('')
        old_format_line_build.append(ac_content_array[headers.index("Estimated National Average Operating Cooling Cost ($)")])
        old_format_line_build.append(ac_content_array[headers.index("Eligible For Federal Tax Credit")])
        if len(ac_content_array[headers.index("AHRI Type")]) == 0:
            old_format_line_build.append(ac_content_array[headers.index("AHRI Type")])
        else:
            if len(x) == 1:
                old_format_line_build.append(' ')
            else:
                old_format_line_build.append(x[1].replace(")",""))

        return old_format_line_build


    def mappingVSMSACContentFiles(self, vsmsac_content_array, headers):
        vsmsac_old_map = []
        vsmsac_old_map.append(vsmsac_content_array[headers.index("AHRI Certified Reference Number")])
        vsmsac_old_map.append(vsmsac_content_array[headers.index("Old AHRI Reference Number")])
        vsmsac_old_map.append(vsmsac_content_array[headers.index("Model Status")])
        vsmsac_old_map.append(vsmsac_content_array[headers.index("Manufacturer Type")])
        vsmsac_old_map.append(vsmsac_content_array[headers.index("Brand Name")])
        vsmsac_old_map.append(vsmsac_content_array[headers.index("Series Name")])
        vsmsac_old_map.append(vsmsac_content_array[headers.index("Brand Name")])
        vsmsac_old_map.append(vsmsac_content_array[headers.index("Outdoor Unit Model Number")])
        vsmsac_old_map.append(vsmsac_content_array[headers.index("Indoor Type")])
        vsmsac_old_map.append(vsmsac_content_array[headers.index("Brand Name")])
        vsmsac_old_map.append(vsmsac_content_array[headers.index("Indoor Model Number(s)")])
        vsmsac_old_map.append('')
        vsmsac_old_map.append('')
        vsmsac_old_map.append(vsmsac_content_array[headers.index("Cooling Capacity (95F)")])
        vsmsac_old_map.append(vsmsac_content_array[headers.index("EER (95F)")])
        vsmsac_old_map.append(vsmsac_content_array[headers.index("SEER")])
        if len(vsmsac_content_array[headers.index("AHRI Type")]) == 0:
            vsmsac_old_map.append(vsmsac_content_array[headers.index("AHRI Type")])
        else:
            x = vsmsac_content_array[headers.index("AHRI Type")].split(" (",1)
            vsmsac_old_map.append(x[0])
        #vsmsac_old_map.append(vsmsac_content_array[headers.index("AHRI Type")].split(" (",1))
        vsmsac_old_map.append(vsmsac_content_array[headers.index("Designated Tested Combination.")])
        vsmsac_old_map.append(vsmsac_content_array[headers.index("Estimated National Average Operating Cooling Cost ($)")])
        vsmsac_old_map.append(vsmsac_content_array[headers.index("Eligible For Federal Tax Credit")])
        if len(vsmsac_content_array[headers.index("AHRI Type")]) == 0:
            vsmsac_old_map.append(vsmsac_content_array[headers.index("AHRI Type")])
        else:
            if len(x) == 1:
                vsmsac_old_map.append(' ')
            else:
                vsmsac_old_map.append(x[1].replace(")",""))
            
        return vsmsac_old_map

    def vsms_ac_header_pos_mapping(self,header_array):
        return 1
        
class FileHPMap:
    def __init__(self, hp_path, vsmshp_path,hp_obsolete_path, vsmshp_obsolete_path):
        self.new_hp_file_path = hp_path
        self.new_vsmshp_path = vsmshp_path
        self.new_hp_bsolete_file_path = hp_obsolete_path
        self.new_vsmshp_obsolete_path = vsmshp_obsolete_path

    def mapHPFile(self):
        rownum = 0
        headers = []
        proc_file_name = self.new_hp_file_path.replace('HP','HP_MAP')
        proc_file_name = proc_file_name.replace('Active','')
        if os.path.isfile(self.new_hp_file_path):
            if os.stat(self.new_hp_file_path).st_size == 0:
                pass
            else:            
                with open(self.new_hp_file_path, 'rb') as f:
                    with open(proc_file_name, 'a') as w:
                        writer = csv.writer(w, quoting=csv.QUOTE_ALL)
                        reader = csv.reader(f)
                        for row in reader:
                            if rownum == 0:
                                headers = row
                            else:
                                writer.writerow(self.mappingHPNewContentFiles(row, headers))
                            rownum += 1


    def mapHPOBSFile(self):
        rownum = 0
        headers = []
        proc_file_name = self.new_hp_bsolete_file_path.replace('HP','HP_MAP')
        proc_file_name = proc_file_name.replace('Obsolete','')
        if os.path.isfile(self.new_hp_bsolete_file_path):
            if os.stat(self.new_hp_bsolete_file_path).st_size == 0:
                pass
            else:            
                with open(self.new_hp_bsolete_file_path, 'rb') as f:
                    with open(proc_file_name, 'a') as w:
                        writer = csv.writer(w, quoting=csv.QUOTE_ALL)
                        reader = csv.reader(f)
                        for row in reader:
                            if rownum == 0:
                                headers = row
                            else:
                                writer.writerow(self.mappingHPNewContentFiles(row, headers))
                            rownum += 1


 
    def mapVSMSHPFile(self):
        rownum = 0
        headers = []
        proc_file_name = self.new_vsmshp_path.replace('VSMSHP','VSMSHP_MAP')
        proc_file_name = proc_file_name.replace('Active','')
        if os.path.isfile(self.new_vsmshp_path):
            if os.stat(self.new_vsmshp_path).st_size == 0:
                pass
            else:
                with open(self.new_vsmshp_path, 'rb') as f:
                    with open(proc_file_name, 'a') as w:
                        writer = csv.writer(w, quoting=csv.QUOTE_ALL)
                        reader = csv.reader(f)
                        for row in reader:
                            if rownum == 0:
                                headers = row
                            else:
                                writer.writerow(self.mappingVSMSHPContentFiles(row, headers))
                            rownum += 1

    def mapVSMSHPOBSFile(self):
        rownum = 0
        headers = []
        proc_file_name = self.new_vsmshp_obsolete_path.replace('VSMSHP','VSMSHP_MAP')
        proc_file_name = proc_file_name.replace('Obsolete','')
        if os.path.isfile(self.new_vsmshp_obsolete_path):
            if os.stat(self.new_vsmshp_obsolete_path).st_size == 0:
                pass
            else:
                with open(self.new_vsmshp_obsolete_path, 'rb') as f:
                    with open(proc_file_name, 'a') as w:
                        writer = csv.writer(w, quoting=csv.QUOTE_ALL)
                        reader = csv.reader(f)
                        for row in reader:
                            if rownum == 0:
                                headers = row
                            else:
                                writer.writerow(self.mappingVSMSHPContentFiles(row, headers))
                            rownum += 1

   
    def mappingHPNewContentFiles(self, hp_content_array, headers):
        old_format_line_build = []
        old_format_line_build.append(hp_content_array[headers.index("AHRI Certified Reference Number")]) 
        old_format_line_build.append(hp_content_array[headers.index("Old AHRI Reference Number")])
        old_format_line_build.append(hp_content_array[headers.index("Model Status")])
        old_format_line_build.append(hp_content_array[headers.index("Manufacturer Type")])
        old_format_line_build.append(hp_content_array[headers.index("Outdoor Unit Brand Name")])
        old_format_line_build.append(hp_content_array[headers.index("Series Name")])
        old_format_line_build.append(hp_content_array[headers.index("Outdoor Unit Brand Name")])
        old_format_line_build.append(hp_content_array[headers.index("Outdoor Unit Model Number  (Condenser or Single Package)")])
        old_format_line_build.append(hp_content_array[headers.index("Indoor Unit Brand Name")])
        old_format_line_build.append(hp_content_array[headers.index("Indoor Unit Model Number (Evaporator and/or Air Handler)")])
        old_format_line_build.append(hp_content_array[headers.index("Indoor Full-Load Air Volume Rate (A2 SCFM)")])
        old_format_line_build.append(hp_content_array[headers.index("Indoor Cooling Intermediate Air Volume Rate (Ev SCFM)")])
        old_format_line_build.append(hp_content_array[headers.index("Indoor Cooling Minimum Air Volume Rate (B1 SCFM)")])
        old_format_line_build.append(hp_content_array[headers.index("Furnace Model Number")])
        old_format_line_build.append(hp_content_array[headers.index("Cooling Capacity (A2) - Single or High Stage (95F),btuh")])
        old_format_line_build.append(hp_content_array[headers.index("EER (A2) - Single or High Stage (95F)")])
        old_format_line_build.append(hp_content_array[headers.index("SEER")])
        old_format_line_build.append(hp_content_array[headers.index("Heating Capacity (H12) - Single or High Stage (47F),btuh")])
        old_format_line_build.append(hp_content_array[headers.index("HSPF (Region IV)")])
        old_format_line_build.append(hp_content_array[headers.index("Heating Capacity (H32) - Single or High Stage (17F),btuh")])
        old_format_line_build.append(hp_content_array[headers.index("Phase")])
        if len(hp_content_array[headers.index("AHRI Type")]) == 0:
            old_format_line_build.append(hp_content_array[headers.index("AHRI Type")])
        else:
            x = hp_content_array[headers.index("AHRI Type")].split(" (",1)
            old_format_line_build.append(x[0])

        old_format_line_build.append(hp_content_array[headers.index("Designated Tested Combination.  ")])
        old_format_line_build.append('')
        old_format_line_build.append(hp_content_array[headers.index("Estimated National Average Operating Cooling Cost ($)")])
        old_format_line_build.append(hp_content_array[headers.index("Estimated National Average Operating Heating Cost ($)")])
        old_format_line_build.append(hp_content_array[headers.index("Eligible For Federal Tax Credit")])
        if len(hp_content_array[headers.index("AHRI Type")]) == 0:
            old_format_line_build.append(hp_content_array[headers.index("AHRI Type")])
        else:
            if len(x) == 1:
                old_format_line_build.append(' ')
            else:
                old_format_line_build.append(x[1].replace(")",""))
        

        
        return old_format_line_build


    def mappingVSMSHPContentFiles(self, vsmshp_content_array, headers):
        vsmshp_old_map = []
        vsmshp_old_map.append(vsmshp_content_array[headers.index("AHRI Certified Reference Number")])
        vsmshp_old_map.append(vsmshp_content_array[headers.index("Old AHRI Reference Number")])
        vsmshp_old_map.append(vsmshp_content_array[headers.index("Model Status")])
        vsmshp_old_map.append(vsmshp_content_array[headers.index("Manufacturer Type")])
        vsmshp_old_map.append(vsmshp_content_array[headers.index("Brand Name")])
        vsmshp_old_map.append(vsmshp_content_array[headers.index("Series Name")])
        vsmshp_old_map.append(vsmshp_content_array[headers.index("Brand Name")])
        vsmshp_old_map.append(vsmshp_content_array[headers.index("Outdoor Unit Model Number")])
        vsmshp_old_map.append(vsmshp_content_array[headers.index("Indoor Type")])
        vsmshp_old_map.append(vsmshp_content_array[headers.index("Brand Name")])
        vsmshp_old_map.append(vsmshp_content_array[headers.index("Indoor Model Number(s)")])
        vsmshp_old_map.append('')
        vsmshp_old_map.append('')
        vsmshp_old_map.append(vsmshp_content_array[headers.index("Cooling Capacity (95F)")])
        vsmshp_old_map.append(vsmshp_content_array[headers.index("EER (95F)")])
        vsmshp_old_map.append(vsmshp_content_array[headers.index("SEER")])
        vsmshp_old_map.append(vsmshp_content_array[headers.index("High Heat (47F)")])
        vsmshp_old_map.append(vsmshp_content_array[headers.index("HSPF")])
        vsmshp_old_map.append(vsmshp_content_array[headers.index("Low Heat (17F)")])
        if len(vsmshp_content_array[headers.index("AHRI Type")]) == 0:
            vsmshp_old_map.append(vsmshp_content_array[headers.index("AHRI Type")])
        else:            
            x = vsmshp_content_array[headers.index("AHRI Type")].split(" (",1)
            vsmshp_old_map.append(x[0])
        #vsmshp_old_map.append(vsmshp_content_array[headers.index("AHRI Type")].split(" (",1))
        vsmshp_old_map.append(vsmshp_content_array[headers.index("Designated Tested Combination.")])
        vsmshp_old_map.append(vsmshp_content_array[headers.index("Estimated National Average Operating Cooling Cost ($)")])
        vsmshp_old_map.append(vsmshp_content_array[headers.index("Estimated National Average Operating Heatling Cost ($)")])
        vsmshp_old_map.append(vsmshp_content_array[headers.index("Eligible For Federal Tax Credit")])
        if len(vsmshp_content_array[headers.index("AHRI Type")]) == 0:
            vsmshp_old_map.append(vsmshp_content_array[headers.index("AHRI Type")])
        else:            
            if len(x) == 1:
                vsmshp_old_map.append(' ')
            else:
                vsmshp_old_map.append(x[1].replace(")",""))
            

        
        return vsmshp_old_map

class FileBoilersMap:
    def __init__(self, cblr_path, rblr_path,cblr_obsolete_path, rblr_obsolete_path):
        self.new_cblr_file_path = cblr_path
        self.new_rblr_path = rblr_path
        self.new_cblr_obsolete_file_path = cblr_obsolete_path
        self.new_rblr_obsolete_path = rblr_obsolete_path

    def mapCBLRfile(self):
        rownum = 0
        headers = []
        proc_file_name = self.new_cblr_file_path.replace('CBLR','CBLR_MAP')
        proc_file_name = proc_file_name.replace('Active','')
        if os.path.isfile(self.new_cblr_file_path):
            if os.stat(self.new_cblr_file_path).st_size == 0:
                pass
            else:
                with open(self.new_cblr_file_path, 'rb') as f:
                    with open(proc_file_name, 'a') as w:
                        writer = csv.writer(w, quoting=csv.QUOTE_ALL)
                        reader = csv.reader(f)
                        for row in reader:
                            if rownum == 0:
                                headers = row
                            else:
                                writer.writerow(self.mappingCBLRNewContentFiles(row, headers))
                            rownum += 1

    def mapCBLROBSFile(self):
        rownum = 0
        headers = []
        proc_file_name = self.new_cblr_obsolete_file_path.replace('CBLR','CBLR_MAP')
        proc_file_name = proc_file_name.replace('Obsolete','')
        if os.path.isfile(self.new_cblr_obsolete_file_path):
            if os.stat(self.new_cblr_obsolete_file_path).st_size == 0:
                pass
            else:
                with open(self.new_cblr_obsolete_file_path, 'rb') as f:
                    with open(proc_file_name, 'a') as w:
                        writer = csv.writer(w, quoting=csv.QUOTE_ALL)
                        reader = csv.reader(f)
                        for row in reader:
                            if rownum == 0:
                                headers = row
                            else:
                                writer.writerow(self.mappingCBLRNewContentFiles(row, headers))
                            rownum += 1


    def mapRBLRFile(self):
        rownum = 0
        headers = []
        proc_file_name = self.new_rblr_path.replace('RBLR','RBLR_MAP')
        proc_file_name = proc_file_name.replace('Active','')
        if os.path.isfile(self.new_rblr_path):
            if os.stat(self.new_rblr_path).st_size == 0:
                pass
            else:
                with open(self.new_rblr_path, 'rb') as f:
                    with open(proc_file_name, 'a') as w:
                        writer = csv.writer(w, quoting=csv.QUOTE_ALL)
                        reader = csv.reader(f)
                        for row in reader:
                            if rownum == 0:
                                headers = row
                            else:
                                writer.writerow(self.mappingRBLRContentFiles(row, headers))
                            rownum += 1

    def mapRBLROBSFile(self):
        rownum = 0
        headers = []
        proc_file_name = self.new_rblr_obsolete_path.replace('RBLR','RBLR_MAP')
        proc_file_name = proc_file_name.replace('Obsolete','')
        if os.path.isfile(self.new_rblr_obsolete_path):
            if os.stat(self.new_rblr_obsolete_path).st_size == 0:
                pass
            else:
                with open(self.new_rblr_obsolete_path, 'rb') as f:
                    with open(proc_file_name, 'a') as w:
                        writer = csv.writer(w, quoting=csv.QUOTE_ALL)
                        reader = csv.reader(f)
                        for row in reader:
                            if rownum == 0:
                                headers = row
                            else:
                                writer.writerow(self.mappingRBLRContentFiles(row, headers))
                            rownum += 1


    def mappingCBLRNewContentFiles(self, cblr_content_array, headers):
        old_format_line_build = []
        old_format_line_build.append(cblr_content_array[headers.index("AHRI Certified Reference Number")])
        old_format_line_build.append(cblr_content_array[headers.index("Old AHRI Reference Number")])
        old_format_line_build.append(cblr_content_array[headers.index("Model Status")])
        old_format_line_build.append(cblr_content_array[headers.index("Brand Name")])
        old_format_line_build.append('')
        old_format_line_build.append(cblr_content_array[headers.index("Model Number")])
        old_format_line_build.append(cblr_content_array[headers.index("Material")])
        old_format_line_build.append(cblr_content_array[headers.index("Location")])
        old_format_line_build.append(cblr_content_array[headers.index("Fuel Type")])
        old_format_line_build.append(cblr_content_array[headers.index("Heating Medium")])
        old_format_line_build.append(cblr_content_array[headers.index("Input Rating, MBH")])
        old_format_line_build.append(cblr_content_array[headers.index("Gross Output (MBH)")])
        old_format_line_build.append(cblr_content_array[headers.index("Combustion Efficiency, %")])
        old_format_line_build.append(cblr_content_array[headers.index("Thermal Efficiency, %")])
        old_format_line_build.append(cblr_content_array[headers.index("Net AHRI Rating, Water (Mbh)")])
        old_format_line_build.append(cblr_content_array[headers.index("Net AHRI Rating, Steam (Mbh)")])
        old_format_line_build.append(cblr_content_array[headers.index("Ignition")])
        old_format_line_build.append(cblr_content_array[headers.index("Draft Type")])
        old_format_line_build.append(cblr_content_array[headers.index("Control")])
        old_format_line_build.append('')
        old_format_line_build.append(cblr_content_array[headers.index("CO2 that is the basis of Ratings, %")])

        

        
        return old_format_line_build


    def mappingRBLRContentFiles(self, rblr_content_array, headers):
        rblr_old_map = []
        rblr_old_map.append(rblr_content_array[headers.index("AHRI Certified Reference Number")])
        rblr_old_map.append(rblr_content_array[headers.index("Old AHRI Reference Number")])
        rblr_old_map.append(rblr_content_array[headers.index("Model Status")])
        rblr_old_map.append(rblr_content_array[headers.index("Brand Name")])
        rblr_old_map.append('')
        rblr_old_map.append(rblr_content_array[headers.index("Model Number")])
        rblr_old_map.append(rblr_content_array[headers.index("Material")])
        rblr_old_map.append(rblr_content_array[headers.index("Location")])
        rblr_old_map.append(rblr_content_array[headers.index("Fuel Type")])
        rblr_old_map.append(rblr_content_array[headers.index("Input Rating, MBH")])
        rblr_old_map.append(rblr_content_array[headers.index("Heating Capacity, MBH")])
        rblr_old_map.append(rblr_content_array[headers.index("AFUE, %")])
        rblr_old_map.append('')
        rblr_old_map.append('')
        rblr_old_map.append('')
        rblr_old_map.append(rblr_content_array[headers.index("Net AHRI Rating, Steam, MBH")])
        rblr_old_map.append(rblr_content_array[headers.index("Net AHRI Rating, Water, MBH")])
        rblr_old_map.append(rblr_content_array[headers.index("Ignition")])
        rblr_old_map.append(rblr_content_array[headers.index("Draft Type")])
        rblr_old_map.append(rblr_content_array[headers.index("Eligible For Federal Tax Credit")])

        

        
        return rblr_old_map

class FileUleMap:
    def __init__(self, ule_path,ule_obsolete_path):
        self.new_ule_file_path = ule_path
        self.new_ule_obsolete_file_path = ule_obsolete_path

    def mapULEfile(self):
        rownum = 0
        headers = []
        proc_file_name = self.new_ule_file_path.replace('ULE','ULE_MAP')
        proc_file_name = proc_file_name.replace('Active','')
        if os.path.isfile(self.new_ule_file_path):
            if os.stat(self.new_ule_file_path).st_size == 0:
                pass
            else:
                with open(self.new_ule_file_path, 'rb') as f:
                    with open(proc_file_name, 'a') as w:
                        writer = csv.writer(w, quoting=csv.QUOTE_ALL)
                        reader = csv.reader(f)
                        for row in reader:
                            if rownum == 0:
                                headers = row
                            else:
                                writer.writerow(self.mappingULENewContentFiles(row, headers))
                            rownum += 1

    def mapULEOBSfile(self):
        rownum = 0
        headers = []
        proc_file_name = self.new_ule_obsolete_file_path.replace('ULE','ULE_MAP')
        proc_file_name = proc_file_name.replace('Obsolete','')
        if os.path.isfile(self.new_ule_obsolete_file_path):
            if os.stat(self.new_ule_obsolete_file_path).st_size == 0:
                pass
            else:
                with open(self.new_ule_obsolete_file_path, 'rb') as f:
                    with open(proc_file_name, 'a') as w:
                        writer = csv.writer(w, quoting=csv.QUOTE_ALL)
                        reader = csv.reader(f)
                        for row in reader:
                            if rownum == 0:
                                headers = row
                            else:
                                writer.writerow(self.mappingULENewContentFiles(row, headers))
                            rownum += 1


    def mappingULENewContentFiles(self, ule_content_array, headers):
        old_format_line_build = []
        old_format_line_build.append(ule_content_array[headers.index("AHRI Certified Reference Number")])
        old_format_line_build.append(ule_content_array[headers.index("Old AHRI Reference Number")])
        old_format_line_build.append(ule_content_array[headers.index("Model Status")])
        old_format_line_build.append(ule_content_array[headers.index("Brand Name")])
        old_format_line_build.append(ule_content_array[headers.index("Series Name")])
        old_format_line_build.append('')
        old_format_line_build.append(ule_content_array[headers.index("Model Number")])
        old_format_line_build.append(ule_content_array[headers.index("Indoor Unit Model Number")])
        old_format_line_build.append(ule_content_array[headers.index("Refrigerant Type")])
        old_format_line_build.append(ule_content_array[headers.index("Cooling Capacity 95F/Cooling Capacity 95F at 230v")])
        old_format_line_build.append(ule_content_array[headers.index("EER 95F/EER 95F at 230v")])
        old_format_line_build.append(ule_content_array[headers.index("IEER/IEER at 230v")])
        old_format_line_build.append(ule_content_array[headers.index("Heating Capacity 47F/Heating Capacity 47F at 230v")])
        old_format_line_build.append(ule_content_array[headers.index("COP 47F/COP 47F at 230v")])
        old_format_line_build.append(ule_content_array[headers.index("Heating Capacity 17F/Heating Capacity 17Fat 230v")])
        old_format_line_build.append(ule_content_array[headers.index("COP 17F/COP 17Fat 230v")])
        if len(ule_content_array[headers.index("AHRI Type")]) == 0:
            old_format_line_build.append(ule_content_array[headers.index("AHRI Type")])
        else:
            x = ule_content_array[headers.index("AHRI Type")].split(" (",1)
            old_format_line_build.append(x[0])
        #old_format_line_build.append(ule_content_array[headers.index("AHRI Type")].split(" (",1))
        old_format_line_build.append(ule_content_array[headers.index("Full Load Indoor Coil Air Quantity (scfm)")])
        if len(ule_content_array[headers.index("AHRI Type")]) == 0:
            old_format_line_build.append(ule_content_array[headers.index("AHRI Type")])
        else:
            if len(x) == 1:
                old_format_line_build.append(' ')
            else:
                old_format_line_build.append(x[1].replace(")",""))
        

        
        return old_format_line_build

class FileFurnaceMap:
    def __init__(self, furnace_path,furnace_obsolete_path):
        self.new_furnace_file_path = furnace_path
        self.new_furnace_obsolete_file_path = furnace_obsolete_path

    def mapRFRNfile(self):
        rownum = 0
        headers = []
        proc_file_name = self.new_furnace_file_path.replace('RFR','RFR_MAP')
        proc_file_name = proc_file_name.replace('Active','')    
        if os.path.isfile(self.new_furnace_file_path):
            if os.stat(self.new_furnace_file_path).st_size == 0:
                pass
            else:
                with open(self.new_furnace_file_path, 'rb') as f:
                    with open(proc_file_name, 'a') as w:
                        writer = csv.writer(w, quoting=csv.QUOTE_ALL)
                        reader = csv.reader(f)
                        for row in reader:
                            if rownum == 0:
                                headers = row
                            else:
                                writer.writerow(self.mappingRFRNNewContentFiles(row, headers))
                            rownum += 1

    def mapRFRNOBSfile(self):
        rownum = 0
        headers = []
        proc_file_name = self.new_furnace_obsolete_file_path.replace('RFR','RFR_MAP')
        proc_file_name = proc_file_name.replace('Obsolete','')
        if os.path.isfile(self.new_furnace_obsolete_file_path):
            if os.stat(self.new_furnace_obsolete_file_path).st_size == 0:
                pass
            else:
                with open(self.new_furnace_obsolete_file_path, 'rb') as f:
                    with open(proc_file_name, 'a') as w:
                        writer = csv.writer(w, quoting=csv.QUOTE_ALL)
                        reader = csv.reader(f)
                        for row in reader:
                            if rownum == 0:
                                headers = row
                            else:
                                writer.writerow(self.mappingRFRNNewContentFiles(row, headers))
                            rownum += 1


    def mappingRFRNNewContentFiles(self, furnace_content_array, headers):
        old_format_line_build = []
        old_format_line_build.append(furnace_content_array[headers.index("AHRI Certified Reference Number")])
        old_format_line_build.append(furnace_content_array[headers.index("Old AHRI Reference Number")])
        old_format_line_build.append(furnace_content_array[headers.index("Model Status")])
        old_format_line_build.append(furnace_content_array[headers.index("Brand Name")])
        old_format_line_build.append('')#old manufactured field
        old_format_line_build.append(furnace_content_array[headers.index("Model Number")])
        old_format_line_build.append('')#old manufactured housing field
        old_format_line_build.append(furnace_content_array[headers.index("Fuel Type")])
        old_format_line_build.append(furnace_content_array[headers.index("Configuration")])
        old_format_line_build.append(furnace_content_array[headers.index("Furnace Type")])
        old_format_line_build.append(furnace_content_array[headers.index("Input Rating (MBTUH)")])
        old_format_line_build.append(furnace_content_array[headers.index("Output Heating Capacity (MBTUH)")])
        old_format_line_build.append(furnace_content_array[headers.index("AFUE, (%) ")])
        old_format_line_build.append(furnace_content_array[headers.index("Ef (MMBTU/yr) ")])
        old_format_line_build.append(furnace_content_array[headers.index("Eae including Eso(kWh/yr)")])
        old_format_line_build.append(furnace_content_array[headers.index("PE (watts) ")])
        old_format_line_build.append('')#old basic model field
        old_format_line_build.append(furnace_content_array[headers.index("Electronically Commutated Motor (ECM)")])
        old_format_line_build.append(furnace_content_array[headers.index("Eligible For Federal Tax Credit")])

        


        return old_format_line_build