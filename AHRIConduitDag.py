import logging
import ahri_conduit.classes.constant as constant
from airflow.operators import PythonOperator, EmailOperator
from airflow.models import DAG
from datetime import datetime, timedelta
from ahri_conduit.classes.OperatorsFunctions import OperatorsFunctions
from ahri_conduit.classes.AHRIConduitGeneralReport import ConduitReportWrap

default_args = {
    'owner': 'oncallair',
    'depends_on_past': False,
    'start_date': datetime(2020, 8, 25),
    'email': constant.CONDUIT_NOTIFICATION_ERROR_LIST,
    'email_on_failure': True,
    'email_on_retry': True,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

logger = logging.getLogger()



dag = DAG('AHRI-Conduit', default_args=default_args,  schedule_interval="30 10 * * 1")

# ---------------------------- #CONDUIT DAG OPERATORS DECLARATIONS ---------------------------------------------------

email_template = """
    {{ params.email_content }}
"""

enterprise_report_instance = ConduitReportWrap(constant.CONDUIT_DB_READ_ENDPOINT, constant.CONDUIT_DB_READ_USER, constant.CONDUIT_DB_READ_PASSWORD, constant.CONDUIT_DB_READ_NAME, constant.CONDUIT_TMP_LOG_PATCH)

def send_enterprise_data_report():
    # this task will read the results of the operation and will create an email with a operation result report for enterprise
    # read and return the report file generated
    return enterprise_report_instance.read_conduit_report_files()

op_get_files_from_bucket = PythonOperator(
    task_id='Reading-and-preparing-new-files',
    provide_context=False,
    python_callable=OperatorsFunctions.get_files_from_bucket,
    dag=dag)

op_ac_mapping_new_source = PythonOperator(
    task_id='Mapping-AC-new-file-content',
    provide_context=False,
    python_callable=OperatorsFunctions.ac_mapping_new_source,
    dag=dag)

op_hp_mapping_new_source = PythonOperator(
    task_id='Mapping-HP-new-file-content',
    provide_context=False,
    python_callable=OperatorsFunctions.hp_mapping_new_source,
    dag=dag)

op_boiler_mapping_new_source = PythonOperator(
    task_id='Mapping-Boiler-new-file-content',
    provide_context=False,
    python_callable=OperatorsFunctions.boiler_mapping_new_source,
    dag=dag)

op_ule_mapping_new_source = PythonOperator(
    task_id='Mapping-ULE-new-file-content',
    provide_context=False,
    python_callable=OperatorsFunctions.ule_mapping_new_source,
    dag=dag)

op_furnace_mapping_new_source = PythonOperator(
    task_id='Mapping-Furnaces-new-file-content',
    provide_context=False,
    python_callable=OperatorsFunctions.furnace_mapping_new_source,
    dag=dag)

op_hp_process_equipment = PythonOperator(
    task_id='Heat-Pump-equipment-processing',
    provide_context=False,
    python_callable=OperatorsFunctions.hp_process_equipment,
    dag=dag)

op_ac_process_equipment = PythonOperator(
    task_id='Straight-Cool-equipment-processing',
    provide_context=False,
    python_callable=OperatorsFunctions.ac_process_equipment,
    dag=dag)

op_f_process_equipment = PythonOperator(
    task_id='Furnace-equipment-processing',
    provide_context=False,
    python_callable=OperatorsFunctions.f_process_equipment,
    dag=dag)

op_b_process_equipment = PythonOperator(
    task_id='Boilers-equipment-processing',
    provide_context=False,
    python_callable=OperatorsFunctions.b_process_equipment,
    dag=dag)

op_ule_process_equipment = PythonOperator(
    task_id='ULE-equipment-processing',
    provide_context=False,
    python_callable=OperatorsFunctions.ule_process_equipment,
    dag=dag)

op_setup_conduit_support = PythonOperator(
    task_id='Conduit-Enterprise-support-setup',
    provide_context=False,
    python_callable=OperatorsFunctions.setup_conduit_support,
    dag=dag)

op_upgrade_models_for_conduit_ac = PythonOperator(
    task_id='Upgrade-Models-Enterprise-AC',
    provide_context=False,
    python_callable=OperatorsFunctions.upgrade_models_for_conduit_ac,
    dag=dag)

op_upgrade_models_for_conduit_ac_vsmsac = PythonOperator(
    task_id='Upgrade-Models-Enterprise-AC-VSMSAC',
    provide_context=False,
    python_callable=OperatorsFunctions.upgrade_models_for_conduit_ac_vsmsac,
    dag=dag)

op_upgrade_models_for_conduit_hp = PythonOperator(
    task_id='Upgrade-Models-Enterprise-HP',
    provide_context=False,
    python_callable=OperatorsFunctions.upgrade_models_for_conduit_hp,
    dag=dag)

op_upgrade_models_for_conduit_hp_vsmshp = PythonOperator(
    task_id='Upgrade-Models-Enterprise-HP-VSMSHP',
    provide_context=False,
    python_callable=OperatorsFunctions.upgrade_models_for_conduit_hp_vsmshp,
    dag=dag)

op_upgrade_models_for_conduit_r_blr = PythonOperator(
    task_id='Upgrade-Models-Enterprise-Boiler-RBLR',
    provide_context=False,
    python_callable=OperatorsFunctions.upgrade_models_for_conduit_r_blr,
    dag=dag)

op_upgrade_models_for_conduit_c_blr = PythonOperator(
    task_id='Upgrade-Models-Enterprise-Boiler-CBLR',
    provide_context=False,
    python_callable=OperatorsFunctions.upgrade_models_for_conduit_c_blr,
    dag=dag)

op_upgrade_models_for_conduit_ule = PythonOperator(
    task_id='Upgrade-Models-Enterprise-ULE',
    provide_context=False,
    python_callable=OperatorsFunctions.upgrade_models_for_conduit_ule,
    dag=dag)

op_upgrade_models_for_conduit_furnace = PythonOperator(
    task_id='Upgrade-Models-Enterprise-FURNACE',
    provide_context=False,
    python_callable=OperatorsFunctions.upgrade_models_for_conduit_furnace,
    dag=dag)

op_upgrade_general_models_for_conduit = PythonOperator(
    task_id='Upgrade-General-Models-Data',
    provide_context=False,
    python_callable=OperatorsFunctions.upgrade_general_models_for_conduit,
    dag=dag)

op_create_enterprise_report = PythonOperator(
    task_id='Create-Enterprise-Report',
    provide_context=False,
    python_callable=OperatorsFunctions.create_enterprise_report,
    dag=dag)

op_enterprise_email_notification = EmailOperator(
    task_id='Send-Enterprise-Data-report',
    to=constant.AHRI_REPORT_USERS_LIST,
    subject='Conduit AHRI Weekly Activity',
    html_content=email_template,
    params={'email_content': send_enterprise_data_report()},
    dag=dag)

op_clean_up_workspace = PythonOperator(
    task_id='Clean-Up-Workspace',
    provide_context=False,
    python_callable=OperatorsFunctions.clean_up_workspace,
    dag=dag)


# ----------------------------COMMON START DAG DEPENDENCIES ------------------------------------------------------------

op_hp_mapping_new_source.set_upstream(op_get_files_from_bucket)
op_ac_mapping_new_source.set_upstream(op_get_files_from_bucket)
op_boiler_mapping_new_source.set_upstream(op_get_files_from_bucket)
op_ule_mapping_new_source.set_upstream(op_get_files_from_bucket)
op_furnace_mapping_new_source.set_upstream(op_get_files_from_bucket)

op_hp_process_equipment.set_upstream(op_hp_mapping_new_source)
op_ac_process_equipment.set_upstream(op_ac_mapping_new_source)
op_f_process_equipment.set_upstream(op_furnace_mapping_new_source)
op_b_process_equipment.set_upstream(op_boiler_mapping_new_source)
op_ule_process_equipment.set_upstream(op_ule_mapping_new_source)

# ----------------------------CONDUIT DAG DEPENDENCIES ---------------------------------------------------------------

op_setup_conduit_support.set_upstream(op_ule_process_equipment)
op_setup_conduit_support.set_upstream(op_b_process_equipment)
op_setup_conduit_support.set_upstream(op_f_process_equipment)
op_setup_conduit_support.set_upstream(op_ac_process_equipment)
op_setup_conduit_support.set_upstream(op_hp_process_equipment)

op_upgrade_models_for_conduit_ac.set_upstream(op_setup_conduit_support)
op_upgrade_general_models_for_conduit.set_upstream(op_upgrade_models_for_conduit_ac)

op_upgrade_models_for_conduit_ac_vsmsac.set_upstream(op_setup_conduit_support)
op_upgrade_general_models_for_conduit.set_upstream(op_upgrade_models_for_conduit_ac_vsmsac)

op_upgrade_models_for_conduit_hp.set_upstream(op_setup_conduit_support)
op_upgrade_general_models_for_conduit.set_upstream(op_upgrade_models_for_conduit_hp)

op_upgrade_models_for_conduit_hp_vsmshp.set_upstream(op_setup_conduit_support)
op_upgrade_general_models_for_conduit.set_upstream(op_upgrade_models_for_conduit_hp_vsmshp)

op_upgrade_models_for_conduit_r_blr.set_upstream(op_setup_conduit_support)
op_upgrade_general_models_for_conduit.set_upstream(op_upgrade_models_for_conduit_r_blr)

op_upgrade_models_for_conduit_c_blr.set_upstream(op_setup_conduit_support)
op_upgrade_general_models_for_conduit.set_upstream(op_upgrade_models_for_conduit_c_blr)

op_upgrade_models_for_conduit_ule.set_upstream(op_setup_conduit_support)
op_upgrade_general_models_for_conduit.set_upstream(op_upgrade_models_for_conduit_ule)

op_upgrade_models_for_conduit_furnace.set_upstream(op_setup_conduit_support)
op_upgrade_general_models_for_conduit.set_upstream(op_upgrade_models_for_conduit_furnace)

op_create_enterprise_report.set_upstream(op_upgrade_general_models_for_conduit)

op_enterprise_email_notification.set_upstream(op_create_enterprise_report)

# ----------------------------COMMON END DAG DEPENDENCIES --------------------------------------------------------------

op_clean_up_workspace.set_upstream(op_enterprise_email_notification)